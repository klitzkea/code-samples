/*
 * gcs.h
 *
 * Creates and manages a connection to G Connected Services
 *
 * Description:
 * Most of the API is pretty straightforward. This manages a connection to GCS to upload FIT files through.
 *
 * External functions must use it in a specific order though. Invalid sequencing causes unspecified behavior.
 *
 * Sequence:
 * GCS_ProcessMessage(OPEN)					  **required**	  required to open the connection to gcs
 * GCS_SendFitFile(len > 0, moredata = true)  **optional**	  any data sent through form will begin a transfer to GCS
 * GCS_SendFitFile(moredata = false)		  **required**    will send any data remaining to be sent, and end the transfer to GCS
 *	repeat
 *
 *  Created on: Jan 6, 2014
 *      Author: andrewk
 */

#ifndef GCS_H_
#define GCS_H_

#include "config\app_resource_config.h"
#include "oauth\oauth.h"
#include "gcs\gcs\gcs_debug.h"

#ifndef SSL_ENABLED
#define GCS_CONN_TYPE		NPE_HTTP	// HTTP or HTTPS
#define GCS_PORT			80
#define GCS_SSL_CERT		(UINT8*)0
#define GCS_SSL_CERT_LEN	0
#define GCS_SSL_CERT_NAME	NULL
#else // ssl enabled
#define GCS_CONN_TYPE		NPE_HTTPS
#define GCS_PORT			443
#define GCS_SSL_CERT		(UINT8*)ca1Cert
#define GCS_SSL_CERT_LEN    ca1CertSize
#define GCS_SSL_CERT_NAME	"CA1-2017"
#endif // ssl

#define GCS_TCP_HOLD_TIME	(ULONG64)5000	// Number of milliseconds to keep a GCS tcp connection open when waiting for another connection.
#define GCS_HTTP_TIMEOUT	15	// Number of seconds to wait before timing out.

#define GCS_RETRY_COUNT     3       // Number of retries for GCS operations.  Used by app_wasp.c.

#define GCS_OAUTH_RENEW_TIMEOUT     (ULONG64)(86400 * 5)     // Number of seconds before renewing OAuth tokens
#define GCS_OAUTH_RETRY_TIMEOUT     (ULONG64)3              // Number of seconds to wait before retrying if OAuth update fails
#define GCS_DEVTABLE_RENEW_TIMEOUT  (ULONG64)(180)          // Number of seconds before renewing Device Table
#define GCS_DEVTABLE_RETRY_TIMEOUT  (ULONG64)3              // Number of seconds to wait before retrying if Device Table update fails
#define GCS_DIAG_UL_TIMEOUT			(ULONG64)(60 * 30)		// Half hour

#define GCS_AUTH_HEADER_SIZE	512	// Number of bytes to allocate for OAuth header

#define GCS_FIT_FILE_BUFF_SIZE  40896 // Odd size to allow for all of the space to be used. Transfer chunks are odd sizes to maintain base-64 streaming

#define DEVICE_TABLE_RX_BUF_SIZE	(0x400 * 4)  	// 4kB
#define REG_BRIDGE_RX_BUF_SIZE		(0x200 * 3)		// 1.5kB
#define FIT_FILE_RX_BUF_SIZE		(0x200 * 3)		// 1.5kB
#define UL_DATA_RX_BUF_SIZE			(0x200 * 3)		// 1.5kB
#define OAUTH_RX_BUF_SIZE			(0x400 * 2)		// 2kB
#define JSON_TX_BUF_SIZE			(64)			// Amount to allocate on stack when sending and building JSON
#define DIAG_TX_BUF_SIZE			(128)

#define DEVICE_TABLE_SIZE	500
#define RECORD_LIMIT_STR    "500"          // Same value as Device Table Size

#define GCS_OAUTH_TOKEN_KEY_SIZE 	128		// Number of bytes for a token key
#define GCS_OAUTH_TOKEN_SECRET_SIZE	256		// Number of bytes for a token secret

#define GCS_STACK_SIZE				0xC00	// size of GCS stack
#define GCS_QUEUE_MSG_COUNT			5300	// Message count size

typedef enum
{
	GCS_MESSAGE_ANT_OPEN,	// Indicates to GCS connection manager that a connection to an ant device has been opened.
} GCS_MESSAGE_T;

// Events sent through the notify callback
typedef enum
{
//    GCS_CONN_OPEN_SUCCESS,
//    GCS_CONN_OPEN_FAILURE,
//    GCS_CONN_ERROR_CLOSE,
//    GCS_SEND_FAIL,
    GCS_SERVICE_READY,
    GCS_SERVICE_DOWN,
    GCS_SERVICE_RESTORED,
} GCS_NOTIFY_EVENT_T;

typedef VOID ( * GCS_CONN_COMPLETE_CB_T )( VOID *pCtx, GCS_NOTIFY_EVENT_T event );

// Defines header locations within internal array's.
// Only reason this structure is placed in this file is so that GCS_CONN_CTX_T can properly define its size.
typedef enum
{
	GCS_HEADER_PROTOCOL = 0,		// Value is static
	GCS_HEADER_CONTENT_TYPE,		// Static
	GCS_HEADER_HOST,				// Static
	GCS_HEADER_TRANSFER_ENCODING,	// Dynamic
	GCS_HEADER_AUTHORIZATION,		// Dynamic
	GCS_HEADER_COUNT
} GCS_HEADER_INDEX_T;

// Context structure for GCS link.
// Only needs to be passed to InitCtx, no need for user to modify any values in this structure.
typedef struct
{
	NPE_HTTPC_CONF_INFO_T http_conf;
	NPE_HTTPC_CONN_HANDLE http_conn;
	UINT8 conn_status;			// GCS_CONNECTION_STATUS_T
	UINT8 gcs_state;			// GCS_MGMT_STATE_T
	UINT8 ext_gcs_state;		// state of the messages streaming into GCS through ProcessMessage and SendFitFile.
	BOOL  ext_gcs_oauth_pending : 1;
	BOOL  ext_gcs_devtable_pending : 1;
	BOOL  ext_gcs_diag_ul_pending : 1;
	GSN_OSAL_MUTEX_T ext_state_mut;	// Mutex protecting access to the external GCS state
	BOOL  service_notify_sent : 1;  // Indicates client hs been notified of service availability
	BOOL  abnormal_close : 1;       // GCS ConnClose request was due to error in socket operations
	BOOL  oauth_avail : 1;          // OAuth tokens have been updated at least once and are available
	BOOL  devtable_pending : 1;     // Device Table update is pending once GCS Conn is available
	BOOL  devtable_avail : 1;       // Device Table has been updated at least once and is available
	BOOL isBooting : 1;
	BOOL oauthFailed : 1;
	BOOL devTableFailed : 1;
	BOOL notify_called : 1; 							// If the notify callback has been called.
	BOOL fitFileSendInProgress : 1;		// Whether or not there is currently a fit file being sent.
	BOOL diagOpen : 1;
	OAUTH_CTX_T oauth_ctx;		// OAuth context
	GSN_SOFT_TMR_T tcp_timer;	// Timer which times tcp connection and whether or not it should be open or closed.
	GSN_SOFT_TMR_T oauth_timer; // Timer for next OAuth token update
	GSN_SOFT_TMR_T devtable_timer;  // Timer for next Device Table update
	GSN_SOFT_TMR_T diag_ul_timer;	// Timer for diagnostics upload

	// Used by above structure
	NPE_HTTP_USER_HEADER_TYPE_T headerTypes[GCS_HEADER_COUNT];	// Pointer to an array of header types.
	INT8* headerValues[GCS_HEADER_COUNT];		// Pointer to an array of pointers to the values of corresponding headers
	struct sockaddr_storage gcs_ip_addr_s;			// gcs socket config
	GSN_NWIF_CTX_T*		nwCtx;					// Active network that GCS should use.

	// User Settings
	INT8 BridgeId[9];
	INT8 consumerTokenKey[GCS_OAUTH_TOKEN_KEY_SIZE];	// OAuth consumer token key buffer
	INT8 consumerTokenSecret[GCS_OAUTH_TOKEN_SECRET_SIZE];	// OAuth consumer token secret buffer
	INT8 tokenKey[GCS_OAUTH_TOKEN_KEY_SIZE];			// OAuth buffer
	INT8 tokenSecret[GCS_OAUTH_TOKEN_SECRET_SIZE];		// OAuth buffer
	APP_CFG_MGR_T* 			configMgr;					// config manager
	UINT8					sysQualId;					// sys qual ID

	// To notify client that connection to GCS has been established
	GCS_CONN_COMPLETE_CB_T  gcsNotifyCb;

	// For buffering FIT files between external API and management thread
	NPE_RING_BUF_T      fitFileRingBuf;
	UINT8   			fitDataFileBuffer[GCS_FIT_FILE_BUFF_SIZE];
	GSN_OSAL_SEM_T		fitFileRingBufSem;			// semaphore for controlling access to the ring buffer
	INT8				fitFileChunksUntilSend;	// How many chunks are left to be sent by the user before GCS sends the data.
	UINT16				currentGcsFitFileCrc;			// Used to check and validate the CRC of the current FIT file as it is being streamed out
	UINT16				currentExtFitFileCrc;			// Used to check validity of incoming files in SendFitFile

	// Thread related variables
	GSN_OSAL_THREAD_TCB_T   gcsManagementThread;
	UINT8					gcsManagementThreadStack[GCS_STACK_SIZE];
	GSN_OSAL_QUEUE_T      	gcsMsgQueue;
	UINT8                  	gcsMgmtThreadMsgQueueBuf[ GCS_QUEUE_MSG_COUNT * sizeof(UINT32) ];

	// For shutting down the thread
	GSN_OSAL_SEM_T* shutdownSem;

	// For debugging.
//	GCS_DBG_CTX_T dbgCtx;
    VIVOHUB_DEBUG_CTX_T  *vivohubDbgCtxPtr;
    DIAG_T*				diagCtx;

    // For tracking connection activity
    KICK_TMR_T			kickTmr;
} GCS_CONN_CTX_T;

/**
 * To be called once upon boot, to initialize the device table and other statically allocated, resource-limited structures.
 */
PUBLIC VOID
GCS_Init( VOID );

// Initializes a GCS connection link, assuming the initial ant connection state is closed.
PUBLIC VOID
GCS_InitCtx(
        GCS_CONN_COMPLETE_CB_T notifyCb,
        GCS_CONN_CTX_T* ctx,
        UINT8 threadPriority,
        UINT8 sysQualId,
        APP_CFG_MGR_T* configMgr,
        GSN_NWIF_CTX_T* nwCtx,
        VIVOHUB_DEBUG_CTX_T *dbgCtx,
        DIAG_T* diagCtx
);

// Turns off timers, threads, and deletes any os-related resources from the OS lists.
PUBLIC VOID
GCS_ShutDown(GCS_CONN_CTX_T* ctx);

// Indicates whether Oauth tokens and device table are available and ready for use.
PUBLIC BOOL
GCS_IsServiceAvailable(GCS_CONN_CTX_T* ctx);

// Notifies the GCS module that the RTC has been updated with an external time server
//   and that it may begin OAuth token and device table update, if it has not done so.
PUBLIC VOID
GCS_NotifyRtcReady(GCS_CONN_CTX_T* ctx);

// Indicates to connection manager that an ant device has either started connecting, or dropped a connection
PUBLIC GSN_STATUS
GCS_ProcessMessage(GCS_MESSAGE_T message, GCS_CONN_CTX_T* ctx);

/**
 * Sends fit data.
 * Data must be sent in multiples of eight.
 * If this is the last message (moreData = 0) then data can be outside a multiple of eight.
 */
PUBLIC GSN_STATUS
GCS_SendFitFile(UINT8* data, INT32 len, BOOL moreData, GCS_CONN_CTX_T* ctx);

/** Wait for there to be at least 1 open spot in the GCS message queue */
PUBLIC VOID
GCS_WaitForSpace(GCS_CONN_CTX_T* ctx);

/**
 * Gets whether or not GCS is recommending beginning another Antfs session
 */
PUBLIC BOOL
GCS_IsReady(GCS_CONN_CTX_T* ctx);

#endif /* GCS_CONN_H_ */
