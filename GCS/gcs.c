/*
 * gcs.c
 *
 *  Created on: Jan 6, 2014
 *      Author: andrewk
 */

#ifdef GSN_DBG_GCS_MODULE_ENABLE
#define GSN_DBG_PER_FILE_ENABLE
#endif

#include "gsn_includes.h"
#include "app_events.h"
#include "app_includes.h"

#include "gcs\gcs.h"
#include "gcs\gcs\gcs_conn.h"
#include "gcs\gcs\gcs_json.h"
#include "gcs\gcs\gcs_device_table.h"
#include "gcs\gcs\gcs_debug.h"
#include "gcs\gcs\gcs_config.h"
#include "gcs\gcs\gcs_oauth.h"
#include "supplicant\utils\base64.h"
#include "encoding\base64_nomalloc.h"
#include "encoding\url.h"
#include "wasp\crc.h"
#include "diag/diag_file.h"

#define GCS_START_TIMER(ctx)	GsnSoftTmr_Start(&(ctx)->tcp_timer, GSN_SOFT_TMR_ONESHOT, 0, MSEC_TO_SYSTIME(GCS_TCP_HOLD_TIME), GCS_ConnectionExpireCB, (ctx))
#define GCS_STOP_TIMER(ctx)		GsnSoftTmr_Stop(&(ctx)->tcp_timer)

#define OAUTH_START_TIMER(ctx, timeoutSec)    \
    GsnSoftTmr_Start(&(ctx)->oauth_timer, \
            GSN_SOFT_TMR_ONESHOT, 0, \
            SEC_TO_SYSTIME(timeoutSec), \
            GCS_OAuthTimerCB, (ctx))
#define OAUTH_STOP_TIMER(ctx)     GsnSoftTmr_Stop(&(ctx)->oauth_timer)

#define DEVTABLE_START_TIMER(ctx, timeoutSec)    \
    GsnSoftTmr_Start(&(ctx)->devtable_timer, \
            GSN_SOFT_TMR_ONESHOT, 0, \
            SEC_TO_SYSTIME(timeoutSec), \
            GCS_DevTableTimerCB, (ctx))
#define DEVTABLE_STOP_TIMER(ctx)     GsnSoftTmr_Stop(&(ctx)->devtable_timer)

#define DIAG_START_TIMER(ctx, timeoutSec)	\
	GsnSoftTmr_Start(&(ctx)->diag_ul_timer, \
			GSN_SOFT_TMR_PERIODIC, 0, \
			SEC_TO_SYSTIME(timeoutSec),  \
			GCS_UlDiagTimerCB, (ctx))
#define DIAG_STOP_TIMER(ctx)     GsnSoftTmr_Stop(&(ctx)->diag_ul_timer)

// Send and receive buffer sizes.
#define SEND_BUFFER_SIZE	64

#define NUM_RECORDS         100
#define NUM_RECORDS_STR    "100"

#define GCS_FIT_FILE_CHUNK_SIZE     8   // 8 bytes is chosen to match FIT file packet burst length
#define GCS_MIN_CHUNKS_PER_SEND     72   // Must be multiple of 3 to allow proper base64 encoding

#define GCS_SYS_QUAL_TIME			30	// Maximum allowed time to process a message before a reset

// States for GCS Management API
typedef enum
{
    GCS_MGMT_STATE_CLOSED,
    GCS_MGMT_STATE_READY,				// Ready for data
    GCS_MGMT_STATE_READY_PENDING,
} GCS_MGMT_STATE_T;

// Events sent between the external api and the gcs management thread
typedef enum
{
    GCS_CONN_OPEN,
    GCS_CONN_CLOSE,
    GCS_DATA_SEND,
    GCS_DATA_END,
    GCS_UPDATE_OAUTH,
    GCS_REGISTER_BRIDGE,
    GCS_UPDATE_DEVTABLE,
    GCS_SEND_DIAGNOSTICS,
    GCS_DNS_LOOKUP,			// Lookup and populate GCS IP address.
    GCS_SHUTDOWN,			// Shutdown GCS, and release the given semaphore.
} GCS_MGMT_MESSAGE_T;

typedef struct
{
	GCS_MGMT_MESSAGE_T 	msg : 8;
	UINT8				bufSize;		// size taken up in buffer
} GCS_MGMT_MSG;

PRIVATE VOID
GCS_ManagementTask(UINT32 arg);

PRIVATE GSN_STATUS
GCS_HandleFitData(GCS_CONN_CTX_T* ctx);

PRIVATE GSN_STATUS
GCS_HandleFitDataEnd(GCS_CONN_CTX_T* ctx, UINT8 size);

PRIVATE GSN_STATUS
GCS_DownloadDeviceInfo(GCS_CONN_CTX_T* ctx);

PRIVATE GSN_STATUS
GCS_RegisterWellnessBridge(GCS_CONN_CTX_T* ctx);

PRIVATE GSN_STATUS
GCS_UlDiagnosticsData(GCS_CONN_CTX_T* ctx);

PRIVATE VOID
GCS_ConnectionExpireCB( VOID* context, GSN_SOFT_TMR_HANDLE_T  timerHandle );

PRIVATE VOID
GCS_OAuthTimerCB( VOID* context, GSN_SOFT_TMR_HANDLE_T  timerHandle );

PRIVATE VOID
GCS_DevTableTimerCB( VOID* context, GSN_SOFT_TMR_HANDLE_T  timerHandle );

PRIVATE VOID
GCS_UlDiagTimerCB( VOID* context, GSN_SOFT_TMR_HANDLE_T  timerHandle );

PRIVATE VOID
GCS_SendMessage(GCS_MGMT_MESSAGE_T msg, UINT8 size, GCS_CONN_CTX_T* ctx);

PUBLIC VOID
GCS_Init( VOID )
{
	GCS_DeviceTableInit();
	GCS_InitJson();
}

// Initializes a GCS connection link, assuming the initial ant connection state is closed.
PUBLIC VOID
GCS_InitCtx(
        GCS_CONN_COMPLETE_CB_T notifyCb,
        GCS_CONN_CTX_T* ctx,
        UINT8 threadPriority,
        UINT8 sysQualId,
        APP_CFG_MGR_T* configMgr,
        GSN_NWIF_CTX_T* nwCtx,
        VIVOHUB_DEBUG_CTX_T *dbgCtx,
        DIAG_T* diagCtx
)
{
	UINT8 mac[4];

	// Initialize entire context structure to zeroes
	memset(ctx, 0x00, sizeof(GCS_CONN_CTX_T));

	GsnOsal_SemCreate(&ctx->fitFileRingBufSem, 0);

	// Initialize states to closed
	ctx->gcs_state = GCS_MGMT_STATE_CLOSED;
	ctx->ext_gcs_state = GCS_MGMT_STATE_CLOSED;
	ctx->service_notify_sent = FALSE;
	ctx->abnormal_close = FALSE;
	ctx->oauth_avail = FALSE;
	ctx->devtable_avail = FALSE;
	ctx->isBooting = TRUE;
	ctx->oauthFailed = FALSE;
	ctx->devTableFailed = FALSE;
	ctx->notify_called = FALSE;
	ctx->diagOpen = FALSE;

	GsnOsal_MutexCreate(&ctx->ext_state_mut, GSN_OSAL_PRI_INHERIT);

	// Copy params
	ctx->sysQualId = sysQualId;
	ctx->configMgr = configMgr;
	ctx->nwCtx = nwCtx;
	ctx->vivohubDbgCtxPtr = dbgCtx;
	ctx->diagCtx = diagCtx;

	// Initialize FIT File buffer
	// Entry size is GCS_FIT_FILE_CHUNK_SIZE bytes
	NpeRingBuf_Init(&ctx->fitFileRingBuf, ctx->fitDataFileBuffer, sizeof(ctx->fitDataFileBuffer), GCS_FIT_FILE_CHUNK_SIZE);

	// Initialize GCS connection
	GCS_InitConn(ctx);
	ctx->fitFileSendInProgress = FALSE;	// Upon initialization, there is no fit file being immediately sent.
	ctx->fitFileChunksUntilSend = GCS_MIN_CHUNKS_PER_SEND;
	ctx->gcsNotifyCb = notifyCb;

    /**< Creating the Mailbox to receive messages */
    GsnOsal_QueueCreate( &ctx->gcsMsgQueue, GsnOsal_QueueMsgSizeInWords(sizeof(GCS_MGMT_MSG)),
    		ctx->gcsMgmtThreadMsgQueueBuf, sizeof( ctx->gcsMgmtThreadMsgQueueBuf ));

    /* Create the GCS Management Task */
    GsnOsal_ThreadCreate(
        GCS_ManagementTask,
        ctx,
        &ctx->gcsManagementThread,
        "GCS Management Task",
        threadPriority,
        (UINT8 *)ctx->gcsManagementThreadStack,
        sizeof(ctx->gcsManagementThreadStack),
        GSN_OSAL_THREAD_INITIAL_READY
    );

    // Read the MAC
    {
    	APP_CONFIG_T* config = CfgMgr_OpenActiveConfig(ctx->configMgr, GSN_OSAL_WAIT_FOREVER);

		// We only want the last three digits, but processor is Little endian. We convert that here.
		mac[0] = config->gsnCfg.networkConfig.macAddr[5];
		mac[1] = config->gsnCfg.networkConfig.macAddr[4];
		mac[2] = config->gsnCfg.networkConfig.macAddr[3];
		mac[3] = 0;

		CfgMgr_CloseActiveConfig(ctx->configMgr);
    }

    // Convert the last 3 digits of the MAC to Hex, and store in bridgeID
    snprintf(ctx->BridgeId, 9, "%X", *((UINT32*)&mac[0]));

    // Print the bridge ID for debugging.
    printf("Bridge ID: %s\r\n", ctx->BridgeId);

    // Initialize OAuth
    GCS_InitOAuth(ctx);

    // Initialize debug stuff
//    GcsDiag_Init(&ctx->dbgCtx);	// commented because noone really uses it, so it's not worth my time to convert right now.

	// Release Semaphore
	GsnOsal_SemRelease(&ctx->fitFileRingBufSem);
}

PUBLIC VOID
GCS_ShutDown(GCS_CONN_CTX_T* ctx)
{
	GSN_OSAL_SEM_T shutdownSem;
	UINT8 msg[4];

	// Create a semaphore which will be used to determine when GCS has shut down.
	GsnOsal_SemCreate(&shutdownSem, 0);

	// assign the semaphore to a variable which can be read by GCS
	ctx->shutdownSem = &shutdownSem;

	// Drain the queue
	while(GSN_SUCCESS == GsnOsal_QueueGet(&ctx->gcsMsgQueue, msg, GSN_OSAL_NO_WAIT));

	// Send the message to shutdown GCS
	GCS_SendMessage(GCS_SHUTDOWN, 0, ctx);

	// Wait for GCS to synchronously shutdown
	GsnOsal_SemAcquire(&shutdownSem, GSN_OSAL_WAIT_FOREVER);

	// Suspend the thread
	GsnOsal_ThreadSuspend(&ctx->gcsManagementThread);

	// Delete the thread
	GsnOsal_ThreadDelete(&ctx->gcsManagementThread);

	// Delete the shutdown semaphore
	GsnOsal_SemDelete(&shutdownSem);
}

PUBLIC BOOL
GCS_IsServiceAvailable(GCS_CONN_CTX_T* ctx)
{
    return ((ctx->oauth_avail) && (ctx->devtable_avail));
}

PUBLIC BOOL
GCS_IsReady(GCS_CONN_CTX_T* ctx)
{
	ULONG queued;
	ULONG avail;
	UINT32 bufCount;

	// Get number of queued messages
	tx_queue_info_get(&ctx->gcsMsgQueue, NULL, &queued, &avail, NULL, NULL, NULL);

	// Acquire ring buf semaphore
	GsnOsal_SemAcquire(&ctx->fitFileRingBufSem, GSN_OSAL_WAIT_FOREVER);
	bufCount = NpeRingBuf_Count(&ctx->fitFileRingBuf);
	GsnOsal_SemRelease(&ctx->fitFileRingBufSem);

	// If either the queue or the ring buffer is over one-quarter full, instruct external functions not to feed GCS with more data.
	if(queued > (GCS_QUEUE_MSG_COUNT / 4) ||
			bufCount > (GCS_FIT_FILE_BUFF_SIZE / GCS_FIT_FILE_CHUNK_SIZE) / 4)
	{
		AppDbg_AsyncPrintf("GCS is throttling. Amt Used: Queue:%d/%d Buf:%d/%d\r\n", queued, queued + avail, bufCount, GCS_FIT_FILE_BUFF_SIZE / GCS_FIT_FILE_CHUNK_SIZE);

		return FALSE;
    }
	else
	{
		// If there is room in both, report GCS as being available
		return TRUE;
	}
}

PUBLIC VOID
GCS_NotifyRtcReady(GCS_CONN_CTX_T* ctx)
{
    // This function is only useful the first time it's called.
    // It's to notify GCS that it may begin acquisition of OAuth
    //   tokens and Device Table.  After that, OAuth and Device
    //   Table updates are managed by the GCS module, and this
    //   function is prevented from causing further updates.
    if( ctx->notify_called )
        return;
    ctx->notify_called = TRUE;

    printf("RTC Ready\r\n");

    if( ctx->isBooting )
    {
    	// Indicate to GCS to do a DNS lookup. This will start the boot chain.
        GCS_SendMessage(GCS_DNS_LOOKUP, 0, ctx);
    }
}

PRIVATE VOID
GCS_ManagementTask(UINT32 arg)
{
    GCS_CONN_CTX_T *    ctx = (GCS_CONN_CTX_T *)arg;
    GSN_STATUS          gsnStatus;
    union
    {
		UINT32              msg32;
		GCS_MGMT_MSG		mgmt;
    } msg;

    /**< Register for system quality monitoring */
    GsnSq_TaskMonitorRegister( ctx->sysQualId, &ctx->gcsManagementThread,
            GCS_SYS_QUAL_TIME );

    /**< Task starting to process a job, start monitoring */
    GsnSq_TaskMonitorStart( ctx->sysQualId, 0xFFFF );

    /**< Handle events forever */
    while( TRUE )
    {
        /**< Task done with job, stop monitoring */
        GsnSq_TaskMonitorStop( ctx->sysQualId );

        /**< wait on message queue */
        GsnOsal_QueueGet( &ctx->gcsMsgQueue, ( UINT8* )&msg,
            GSN_OSAL_WAIT_FOREVER );

        /* GSN_DBG_INFO( msg ); */
        /**< Task starting to process a job, start monitoring */
        GsnSq_TaskMonitorStart( ctx->sysQualId, (INT32)msg.mgmt.msg );

        ThreadTrk_StartEvent(ctx->sysQualId, msg.mgmt.msg);

        /**< Process the message */
        switch(msg.mgmt.msg) {
        case GCS_CONN_OPEN:

            // First, if we receive an open message, we stop the pending close timer from executing.
        	GCS_STOP_TIMER(ctx);

			// Attempt to open a connection to GCS. If the connection is already open, this will just return a success.
			gsnStatus = GCS_ConnOpen(ctx);

			if( GSN_SUCCESS == gsnStatus )
			{
				// We are ready to process data
				ctx->gcs_state = GCS_MGMT_STATE_READY;
			}
        	else
        	{
        		// If we failed, Toss the pending messages until another open message comes in.
				ctx->gcs_state = GCS_MGMT_STATE_CLOSED;

				AppDbg_Printf("GCS_CONN_OPEN Failure: 0x%08X\r\n", gsnStatus);
			}

            break;
        case GCS_CONN_CLOSE:

        	// Close the connection to GCS. If the connection is already closed, this will just return a success.
        	gsnStatus = GCS_ConnClose(ctx);

        	if(gsnStatus != GSN_SUCCESS)
        	{
        		AppDbg_Printf("GCS_CONN_CLOSE Failure: 0x%08X\r\n", gsnStatus);
        	}
        	else
        	{
        		AppDbg_Printf("GCS Connection Closed\r\n");
        	}

        	break;
        case GCS_DATA_SEND:

        	// Process the data that is currently in the ring buffer from this message
            gsnStatus = GCS_HandleFitData(ctx);

            // If the connection is closed, it means a previous error has occurred, which had a different return value, and the connection was auto-closed.
            if(gsnStatus != GSN_SUCCESS)
            {
            	if(gsnStatus != APP_CONNECTION_CLOSED)
            	{
            		AppDbg_AsyncPrintf("HandleFitData Failure: 0x%08X\r\n", gsnStatus);
            	}
            }

            break;

        case GCS_DATA_END:
        	// Process the data that is currently in the ring buffer from this message
        	gsnStatus = GCS_HandleFitDataEnd(ctx, msg.mgmt.bufSize);

        	// If the connection is closed, it means a previous error has occurred, which had a different return value, and the connection auto-closed so we don't need to repeatedly notify the higher-level function.
            if(gsnStatus != GSN_SUCCESS)
            {
            	if(gsnStatus != APP_CONNECTION_CLOSED)
            	{
            		AppDbg_Printf("HandleFitDataEnd Failure: 0x%08X\r\n", gsnStatus);
            	}
            }

            // Indicates that GCS is closed until another 'open' message comes in. This is independent of the actual status of the connection.
            ctx->gcs_state = GCS_MGMT_STATE_CLOSED;

            // A data end message indicates the end of a transfer. We want to start a timer here. If this timer expires, it will post a message to close the GCS connection.
            // This timer is stopped by an 'open' message coming in.
            GCS_START_TIMER(ctx);

        	break;

        case GCS_UPDATE_OAUTH:

        	// Report to up timer that the service is now down
        	UpTmr_ReportDown();

        	// Ensure we are disconnected
        	GCS_ConnClose(ctx);

        	printf("Starting OAUTH\r\n");
            gsnStatus = GCS_GetNewOAuthToken(ctx);
            ctx->gcs_state = GCS_MGMT_STATE_CLOSED;

            // Report to diagnostics
            Diag_WriteOAuthAttempt(ctx->diagCtx, (GSN_SUCCESS == gsnStatus) ? RESULT_PASS : RESULT_FAIL);

            if( GSN_SUCCESS == gsnStatus )
            {
            	printf("OAuth OK\r\n");

				OAUTH_START_TIMER(ctx, GCS_OAUTH_RENEW_TIMEOUT);

            	if(ctx->isBooting)
            	{
					ctx->oauth_avail = TRUE;

					// Now we register as a bridge
					GCS_SendMessage(GCS_REGISTER_BRIDGE, 0, ctx);
            	}
                else
                {
                	// Report that we are up
                	UpTmr_ReportUp();

                    if(ctx->oauthFailed)
                    {
                        // OAuth service was recovered
                        ctx->oauthFailed = FALSE;
                        if((!ctx->devTableFailed) && (ctx->gcsNotifyCb))
                        {
                            (ctx->gcsNotifyCb)(ctx, GCS_SERVICE_RESTORED);
                        }
                    }
                }
            }
            else
            {
            	printf("Oauth failed: 0x%08X\r\n", gsnStatus);
                if(!ctx->isBooting)
                {
                    if( ctx->gcsNotifyCb )
                    {
                        (ctx->gcsNotifyCb)(ctx, GCS_SERVICE_DOWN);
                    }
                    ctx->oauthFailed = TRUE;
                }

                OAUTH_START_TIMER(ctx, GCS_OAUTH_RETRY_TIMEOUT);
            }
            break;

        case GCS_REGISTER_BRIDGE:

        	printf("Registering Bridge\r\n");

        	// Register this device with G
        	gsnStatus = GCS_RegisterWellnessBridge(ctx);

        	// Report attempt to diagnostics
        	Diag_WriteBridgeRegistration(ctx->diagCtx, (gsnStatus == GSN_SUCCESS) ? RESULT_PASS : RESULT_FAIL);

        	// Check result
        	if(gsnStatus != GSN_SUCCESS)
        	{
        		printf("Device Registration Failed: 0x%08X\r\n", gsnStatus);

        		// re-register
        		GCS_SendMessage(GCS_REGISTER_BRIDGE, 0, ctx);

        	}
        	else	// succeeded to register
        	{
        		printf("Bridge Registered\r\n");

        		if( ctx->isBooting ) // Update the device table
        		{
        			GCS_SendMessage(GCS_UPDATE_DEVTABLE, 0, ctx);
            	}
        	}

        	break;

        case GCS_UPDATE_DEVTABLE:

        	// Report that we are out of service while the update takes place
        	UpTmr_ReportDown();

        	printf("Starting Devtable\r\n");
            gsnStatus = GCS_DownloadDeviceInfo(ctx);
            ctx->gcs_state = GCS_MGMT_STATE_CLOSED;

            // Report to diagnostics
            Diag_WriteDevTableUpdate(ctx->diagCtx, (gsnStatus == GSN_SUCCESS) ? RESULT_PASS : RESULT_FAIL);

            if( GSN_SUCCESS == gsnStatus )
            {
            	printf("Device Table Updated OK. Count: %d\r\n", GCS_GetCcfCount());

                ctx->devtable_avail = TRUE;

                DEVTABLE_START_TIMER(ctx, GCS_DEVTABLE_RENEW_TIMEOUT);

                // Report to up-timer that we are up.
				UpTmr_ReportUp();

                if(ctx->isBooting)
                {
					// If we are booting due to an exception, send diagnostics immediately.
                	// There is a 'sort of' bug here in that, if we send the diagnostics due to an exception boot, then we lose association,
                	// we'll always resend it after re-associating. Not a huge deal, though, to re-send after re-association.
                	if(GSN_BOOT_REASON_GET() == GSN_WIF_SYS_BOOT_EXCEPTION_BOOT)
                		GCS_SendMessage(GCS_SEND_DIAGNOSTICS, 0, ctx);

					// Start diagnostics timer. Since this timer is periodic, we only start it a single time upon boot.
					DIAG_START_TIMER(ctx, GCS_DIAG_UL_TIMEOUT);

					if( ctx->gcsNotifyCb )
					{
						(ctx->gcsNotifyCb)(ctx, GCS_SERVICE_READY);
					}
					ctx->isBooting = FALSE; // We are done booting GCS.
                }
                else
                {
                    if(ctx->devTableFailed)
                    {
                        // DevTable service was recovered
                        ctx->devTableFailed = FALSE;
                        if((!ctx->oauthFailed) && (ctx->gcsNotifyCb))
                        {
                            (ctx->gcsNotifyCb)(ctx, GCS_SERVICE_RESTORED);
                        }
                    }
                }
            }
            else
            {
            	printf("Device Table Update Failure: 0x%X\r\n", gsnStatus);
            	if(!ctx->isBooting)
            	{
                    if( ctx->gcsNotifyCb )
                    {
                        (ctx->gcsNotifyCb)(ctx, GCS_SERVICE_DOWN);
                    }
            	    ctx->devTableFailed = TRUE;
            	}

                DEVTABLE_START_TIMER(ctx, GCS_DEVTABLE_RETRY_TIMEOUT);
            }

            // We want to start a timer here. If this timer expires, it will post a message to close the GCS connection.
            // This timer is stopped by an 'open' message coming in.
            GCS_START_TIMER(ctx);

            break;

        case GCS_SEND_DIAGNOSTICS:
        {
        	ULONG64 time;

        	printf("Sending Diagnostics Report...\r\n");

        	// Report that we are down while diagnostics is sending
        	UpTmr_ReportDown();

        	time = GsnSoftTmr_CurrentSystemTime();

        	gsnStatus = GCS_UlDiagnosticsData(ctx);

        	time = (UINT32)SYSTIME_TO_MSEC(((ULONG64)GsnSoftTmr_CurrentSystemTime() - time));

        	// Report to diagnostics
        	Diag_WriteDiagUpload(ctx->diagCtx, (gsnStatus == GSN_SUCCESS) ? RESULT_PASS : RESULT_FAIL);

        	// Report up because we wait the full timeout before re-trying, so GCS could still be up even if diag failed.
        	UpTmr_ReportUp();

        	if(gsnStatus == GSN_SUCCESS)
        	{
        		printf("Diag report OK. Time: %dms\r\n", (UINT32)time);
        	}
        	else
        	{
        		printf("Diag UL failure: 0x%08X\r\n", gsnStatus);
        	}

            // We want to start a timer here. If this timer expires, it will post a message to close the GCS connection.
            // This timer is stopped by an 'open' message coming in.
            GCS_START_TIMER(ctx);

        	break;
        }
        // Perform a DNS lookup of the GCS hostname
        case GCS_DNS_LOOKUP:
		{
			APP_CONFIG_T* config;
			UINT8 hostnameCopy[sizeof(config->waspConfig.GcsHostName)];

			// Open configuration
			config = CfgMgr_OpenActiveConfig(ctx->configMgr, GSN_OSAL_WAIT_FOREVER);

			// Copy out hostname
			memcpy(hostnameCopy, config->waspConfig.GcsHostName, sizeof(hostnameCopy));

			// Close configuration
			CfgMgr_CloseActiveConfig(ctx->configMgr);

        	printf("GCS Dns Lookup on %s\r\n", hostnameCopy);
        	while(GcsCfg_GetGcsIp(&ctx->gcs_ip_addr_s.addr.ipv4.sin_addr, ctx) != GSN_SUCCESS)
        		GsnTaskSleep(2000); // Wait two seconds between retries. Must succeeded before processing any other messages. Failure cannot be recovered from, although unlikely as a DNS lookup has to succeed for time sync.

        	printf("GCS IP: %d.%d.%d.%d\r\n",
        			((UINT8*)&ctx->gcs_ip_addr_s.addr.ipv4.sin_addr.s_addr)[0],
        			((UINT8*)&ctx->gcs_ip_addr_s.addr.ipv4.sin_addr.s_addr)[1],
        			((UINT8*)&ctx->gcs_ip_addr_s.addr.ipv4.sin_addr.s_addr)[2],
        			((UINT8*)&ctx->gcs_ip_addr_s.addr.ipv4.sin_addr.s_addr)[3]);

        	GCS_SendMessage(GCS_UPDATE_OAUTH, 0, ctx);
        	break;
        }
        case GCS_SHUTDOWN:
        	// Stop timers
        	GCS_STOP_TIMER(ctx);
        	DEVTABLE_STOP_TIMER(ctx);
        	OAUTH_STOP_TIMER(ctx);
        	DIAG_STOP_TIMER(ctx);
        	KickTmr_Stop(&ctx->kickTmr);

        	// Delete OS resources
        	GsnOsal_MutexDelete(&ctx->ext_state_mut);
        	GsnOsal_SemDelete(&ctx->fitFileRingBufSem);
        	GsnOsal_QueueDelete(&ctx->gcsMsgQueue);

        	// Stop task monitor
            GsnSq_TaskMonitorStop( ctx->sysQualId );

            // Release shutdown semaphore
            GsnOsal_SemRelease(ctx->shutdownSem);

            // Indicate to UpTmr that we are now down
            UpTmr_ReportDown();

            // Return
        	return;
        }

        // Stop thread tracking
        ThreadTrk_EndEvent(ctx->sysQualId);
    }
}

PUBLIC GSN_STATUS
GCS_ProcessMessage(GCS_MESSAGE_T message, GCS_CONN_CTX_T* ctx)
{
    GSN_STATUS  retVal = GSN_SUCCESS;

	if( !GCS_IsServiceAvailable(ctx) )
	    return GSN_NOT_INITIALIZED;

	// If the message was that an ant connection to a wellness device was opened.
	if(message == GCS_MESSAGE_ANT_OPEN)
	{
		GsnOsal_MutexGet(&ctx->ext_state_mut, GSN_OSAL_WAIT_FOREVER);

		if(ctx->ext_gcs_state == GCS_MGMT_STATE_CLOSED)
		{
			ctx->ext_gcs_state = GCS_MGMT_STATE_READY;

			GsnOsal_MutexPut(&ctx->ext_state_mut);

			GCS_SendMessage(GCS_CONN_OPEN, 0, ctx);
		}
		else
		{
			GsnOsal_MutexPut(&ctx->ext_state_mut);
		}
	}

	return retVal;
}

PUBLIC GSN_STATUS
GCS_SendFitFile(UINT8* data, INT32 len, BOOL moreData, GCS_CONN_CTX_T* ctx)
{
    GSN_STATUS  retVal;

    // Get the state mutex
    GsnOsal_MutexGet(&ctx->ext_state_mut, GSN_OSAL_WAIT_FOREVER);

    // Check external state first, and exit early if we're already closed
    if(ctx->ext_gcs_state == GCS_MGMT_STATE_CLOSED)
    {
    	// Release the state mutex
    	GsnOsal_MutexPut(&ctx->ext_state_mut);

    	return GSN_FAILURE;
    }

	// Release the state mutex
	GsnOsal_MutexPut(&ctx->ext_state_mut);

	GsnOsal_SemAcquire(&ctx->fitFileRingBufSem, GSN_OSAL_WAIT_FOREVER);

	while(len >= GCS_FIT_FILE_CHUNK_SIZE)
	{
		// Place bytes in a buffer
		retVal = NpeRingBuf_Put(&ctx->fitFileRingBuf, data);

		// Check result
		if(retVal != GSN_SUCCESS)
		{
			retVal = GSN_BUFFER_OVERFLOW;
			goto SendFitFileExit;
		}

		// Send a message to the GCS thread to send some of the buffer.
		GCS_SendMessage(GCS_DATA_SEND, GCS_FIT_FILE_CHUNK_SIZE, ctx);

		if(retVal != GSN_SUCCESS)
		{
			goto SendFitFileExit;
		}

		// Update CRC
		ctx->currentExtFitFileCrc = CRC_UpdateCRC16(ctx->currentExtFitFileCrc, data, 8);

		// increment/decrement offset.
		data += GCS_FIT_FILE_CHUNK_SIZE;
		len -= GCS_FIT_FILE_CHUNK_SIZE;
	}

	// If we have no more data,
	if(!moreData)
	{
		// If, on the last message, we have a few leftover bytes
		if(len > 0)
		{
			INT8 buf[GCS_FIT_FILE_CHUNK_SIZE];

			memcpy(buf, data, len); // Copy leftover bytes.

			memset(buf + len, 0x00, GCS_FIT_FILE_CHUNK_SIZE - len); // Fill the rest with zeroes.

			retVal = NpeRingBuf_Put(&ctx->fitFileRingBuf, (UINT8*)buf); // Add leftover data

			// Update CRC
			ctx->currentExtFitFileCrc = CRC_UpdateCRC16(ctx->currentExtFitFileCrc, data, len);
		}
		else // no leftover bytes
		{
			retVal = GSN_SUCCESS; 					// Just indicate to next step that this operation was a success.
		}

		if(retVal == GSN_SUCCESS)
		{
			// Check the CRC
	        if(retVal != GSN_SUCCESS)
	        {
	            goto SendFitFileExit;
	        }

			if(ctx->currentExtFitFileCrc == 0)
			{
				printf("SendFitFile CRC Passed\r\n");
			}
			else
			{
				// Record failure in diagnostics
				VivohubDiag_RecordGcsFailureEvent(ctx->vivohubDbgCtxPtr, VIVOHUB_DIAG_GCS_SFF_CRC_FAIL);

				printf("SendFitFile CRC Failure: 0x%04X\r\n", ctx->currentExtFitFileCrc);
				ctx->currentExtFitFileCrc = 0;
			}

			// Send a message to GCS to indicate we have buffered an ending message
			GCS_SendMessage(GCS_DATA_END, (UINT8)len, ctx);

			// if we have no more data, set the state of the incoming messages to closed, so we cannot receive another data or close message until an open message is queued
			GsnOsal_MutexGet(&ctx->ext_state_mut, GSN_OSAL_WAIT_FOREVER);

			// Check if we're in any state that indicates that we need to post a message to update the dev table or oauth credentials
			// The logic behind this is that, if a transfer is currently in progress and we have a devtable or oauth timer expire, then
			// we can't post a message and interrupt the transfer. So instead we set the state to a 'pending' state, when fininshing the transfer
			// we check to see if any of these events are pending.

			// If we have a periodic update pending.
			if(ctx->ext_gcs_state == GCS_MGMT_STATE_READY_PENDING)
			{
				if(ctx->ext_gcs_devtable_pending)
					GCS_SendMessage(GCS_UPDATE_DEVTABLE, 0, ctx);	// Send a message to update the device table

				if(ctx->ext_gcs_oauth_pending)
					GCS_SendMessage(GCS_UPDATE_OAUTH, 0, ctx);

				if(ctx->ext_gcs_diag_ul_pending)
					GCS_SendMessage(GCS_SEND_DIAGNOSTICS, 0, ctx);

				ctx->ext_gcs_devtable_pending = FALSE;
				ctx->ext_gcs_oauth_pending = FALSE;
				ctx->ext_gcs_diag_ul_pending = FALSE;

			}

			// The management state is now closed, so we set it to closed.
			ctx->ext_gcs_state = GCS_MGMT_STATE_CLOSED;

			GsnOsal_MutexPut(&ctx->ext_state_mut);

			// Set length to zero, so that it reflects the number of bytes remaining to send, and we pass our final check
			len = 0;

		}
		else
		{
			retVal = GSN_BUFFER_OVERFLOW;
			goto SendFitFileExit;
		}
	}

	// If there are leftover bytes, data size was irregular and we want to return an error.
	if(len > 0)
	{
		retVal = GSN_FAILURE;
		goto SendFitFileExit;
	}


SendFitFileExit:

	if(retVal != GSN_SUCCESS)
	{
		ctx->currentExtFitFileCrc = 0; 	// reset CRC
		AppDbg_AsyncPrintf("ERROR: SEND FIT FILE FAILURE: 0x%08X\r\n", retVal);
	}

	GsnOsal_SemRelease(&ctx->fitFileRingBufSem);

	return retVal;
}

PUBLIC VOID
GCS_WaitForSpace(GCS_CONN_CTX_T* ctx)
{
	ULONG available = 0;

	AppDbg_Printf("Waiting for GCS buffer space...\r\n");

	while(available == 0)
	{
		GsnTaskSleep(3);

		tx_queue_info_get(&ctx->gcsMsgQueue, NULL, NULL, &available, NULL, NULL, NULL);
	}

	AppDbg_Printf("Done Waiting\r\n");
}

PRIVATE INLINE GSN_STATUS
GCS_HandleFitData(GCS_CONN_CTX_T* ctx)
{
	GSN_STATUS retval = GSN_SUCCESS;
	INT32 chunksToRead = 0;

//	AppDbg_AsyncPrintf("-");
	// A single fit data entry indicates that the diag is open. here, we open it.
	ctx->diagOpen = TRUE;

	// First, we calculate the chunks to read from the ringbuf,
	// so if something goes awry later, we can maintain the ability to sync with the ring buffer.
	ctx->fitFileChunksUntilSend--;
	if(ctx->fitFileChunksUntilSend == 0)
		chunksToRead = GCS_MIN_CHUNKS_PER_SEND;

	// Guard conditions. If we are not ready to send
	if(ctx->gcs_state != GCS_MGMT_STATE_READY)
	{
		// If the transfer isn't in progress, then this is the first 'data' message of a transfer. if we are closed on the first data message, it means
		// that we failed to connect. Insert a connect error here. We can't insert it earlier due to the fact that GCS does not have control over the
		// diagnostics stream until the ANT state machine begins sending data messages.
		if(!ctx->fitFileSendInProgress)
		{
			VivohubDiag_RecordGcsFailureEvent(ctx->vivohubDbgCtxPtr, VIVOHUB_DIAG_GCS_CONNECT_ERROR);
		}

		retval = APP_CONNECTION_CLOSED;
		goto HandleFitDataExit;
	}

    // If send state is fresh, then we need to send our headers!
    if(!ctx->fitFileSendInProgress)
    {
    	INT8 rxBuffer[32];
    	UINT32 offset;

        // Copy all json data into our send buffer
        offset = 0;

        do
        {
            UINT32 i = 32;

            // Get header
            GCS_GetFitFileJsonHeader(rxBuffer, &i, offset, ctx);

            // Increment offset by the number of bytes we just received
            offset += i;

            // Send all data received from Json Header
            if(i > 0)
            {
                if(GSN_SUCCESS != (retval = GCS_ConnSendAtomic(rxBuffer, i, 0x01, 0x00, 0x00, ctx))) // Don't register a receive buffer yet.
                {
    				// If CSA fails, we need to set the management state to closed
    				ctx->gcs_state = GCS_MGMT_STATE_CLOSED;

    				// Record failure in diagnostics
    				VivohubDiag_RecordGcsFailureEvent(ctx->vivohubDbgCtxPtr, VIVOHUB_DIAG_GCS_SEND_FAIL);

                    goto HandleFitDataExit;   // Free buf and ptr, then exit
                }
            }
            else
            {
                ctx->fitFileSendInProgress = TRUE;
                ctx->fitFileChunksUntilSend = GCS_MIN_CHUNKS_PER_SEND - 1;		// minus one because, well, this message was the first one
                ctx->currentGcsFitFileCrc = 0; 									// reset CRC
                chunksToRead = 0;												// reset chunksToRead, as we shouldn't read any chunks if this is our first msg
                break; // Break when we have no more header to send.
            }
        }
        while(1);
    }

	// If we need to send data
	if(chunksToRead > 0)
	{
		INT8*  	encodedSendBuf; 					// For storing base64-encoded data.
		UINT32 	encodedSendBufLen = 0;

		// Read each entry into our send buffer
		{
			UINT16 	i = 0;														// Tracks offset within buffer
			UINT8   sendBuf[GCS_FIT_FILE_CHUNK_SIZE * GCS_MIN_CHUNKS_PER_SEND]; // For reading from the ring buffer.

			// Loop the number of times a chunk is, incrementing ChunksUntilSend each time.
			while(chunksToRead > 0)
			{
				// First, we attempt to read an entry from the data buffer
				// Acquire semaphore
				GsnOsal_SemAcquire(&ctx->fitFileRingBufSem, GSN_OSAL_WAIT_FOREVER);

				// Read bytes into sendBuf
				retval = NpeRingBuf_Get(&ctx->fitFileRingBuf, sendBuf + (i * GCS_FIT_FILE_CHUNK_SIZE));

				// Release Semaphore
				GsnOsal_SemRelease(&ctx->fitFileRingBufSem);

				// Check status of read
				if(retval != GSN_SUCCESS)
					goto HandleFitDataExit;

				// Inc/dec trackers
				chunksToRead--;
				i++;

				// increment ChunksUntilSend back to it's original state. At the end of the for() loop, it should equal GCS_MIN_CHUNKS_PER_SEND
				ctx->fitFileChunksUntilSend++;
			}

			// Update CRC
			ctx->currentGcsFitFileCrc = CRC_UpdateCRC16(ctx->currentGcsFitFileCrc, (volatile void*)sendBuf, sizeof(sendBuf));

			// Encode data, copy into buf
			encodedSendBuf = (INT8*)base64_encode(sendBuf, GCS_FIT_FILE_CHUNK_SIZE * GCS_MIN_CHUNKS_PER_SEND, &encodedSendBufLen);
		}

		// Send buffer
		if(encodedSendBufLen > 0)
		{
			// Replace base64 encoding with special characters where required, and remove ='s
			Url_Base64Replace(encodedSendBuf, &encodedSendBufLen);

			// Send buffer
			retval = GCS_ConnSendAtomic(encodedSendBuf, encodedSendBufLen, 0x01, 0x00, 0x00, ctx);

			// Only release if length is > 0, otherwise it wasn't allocated in the first place
			gsn_free(encodedSendBuf);

			// Check send result
			if(retval != GSN_SUCCESS)
			{
				// If CSA fails, we need to set the management state to closed
				ctx->gcs_state = GCS_MGMT_STATE_CLOSED;

				// Record failure in diagnostics
				VivohubDiag_RecordGcsFailureEvent(ctx->vivohubDbgCtxPtr, VIVOHUB_DIAG_GCS_SEND_FAIL);

				goto HandleFitDataExit;
			}
		}
		else // Encoding failed. Exit
		{
			retval = GSN_FAILURE;
			goto HandleFitDataExit;
		}

	}

    retval = GSN_SUCCESS;

HandleFitDataExit:

	// If we are trying to exit, and not synced with our ring buffer
	while(chunksToRead > 0)
	{
		UINT8 buf[GCS_FIT_FILE_CHUNK_SIZE];

		GsnOsal_SemAcquire(&ctx->fitFileRingBufSem, GSN_OSAL_WAIT_FOREVER);

		NpeRingBuf_Get(&ctx->fitFileRingBuf, buf);

		GsnOsal_SemRelease(&ctx->fitFileRingBufSem);

		chunksToRead--;
		ctx->fitFileChunksUntilSend++;
	}

    return retval;
}

PRIVATE GSN_STATUS
GCS_HandleFitDataEnd(GCS_CONN_CTX_T* ctx, UINT8 size)
{
	GSN_STATUS retval;
	UINT32 chunksToRead = 0; // Used to ensure sync with ring buffer, in case we prematurely exit this function.
	INT8* rxBuf = 0;		// Used to allocate later if we're receiving a response.

	// First, we calculate the chunks to read, in case we prematurely exit before reading from the ring buffer.
	{
		// Calculate number of entries left in ring buffer before it's ready to send
		chunksToRead = GCS_MIN_CHUNKS_PER_SEND - ctx->fitFileChunksUntilSend;

		// Add an extra entry if there's an odd number of bytes
		if(size > 0)
			chunksToRead++;
	}

	// Guard conditions
	if(ctx->gcs_state != GCS_MGMT_STATE_READY)
	{
		if(ctx->diagOpen)
			VivohubDiag_RecordGcsSessionEnd(ctx->vivohubDbgCtxPtr, VS_RESULT_GCS_CONN_FAIL);
		ctx->diagOpen = FALSE;

		retval = APP_CONNECTION_CLOSED;
		goto HandleFitDataEndExit;
	}

	if(ctx->fitFileSendInProgress)
	{
		// Send any remaining data
		{
			UINT8 buf[GCS_MIN_CHUNKS_PER_SEND * GCS_FIT_FILE_CHUNK_SIZE]; // Worst-case scenario is a full transfer.
			UINT32 len = 0; // Length of data in buffer

			// Get any remaining chunks that need to be sent.
			while((GCS_MIN_CHUNKS_PER_SEND - ctx->fitFileChunksUntilSend) > 0)
			{
				// Get data from buffer
				GsnOsal_SemAcquire(&ctx->fitFileRingBufSem, GSN_OSAL_WAIT_FOREVER);

				retval = NpeRingBuf_Get(&ctx->fitFileRingBuf, &buf[len]);

				GsnOsal_SemRelease(&ctx->fitFileRingBufSem);

				// Check return value
				if(retval != GSN_SUCCESS)
				{
					retval = GSN_BUFFER_OVERFLOW; // assume a buffer overflow has occurred in the past if we can no longer read from the buffer
					goto HandleFitDataEndExit;
				}
				else // read success
				{
					chunksToRead--; 				// Indicate we have read a chunk
					len += GCS_FIT_FILE_CHUNK_SIZE;	// increment length
					ctx->fitFileChunksUntilSend++;	// increment number of chunks until send, since we've read from the send buffer.
				}
			}

			// if we have leftover data to send, add it to the buffer
			if(size > 0)
			{
				// Read in data at offset
				GsnOsal_SemAcquire(&ctx->fitFileRingBufSem, GSN_OSAL_WAIT_FOREVER);

				retval = NpeRingBuf_Get(&ctx->fitFileRingBuf, &buf[len]);

				GsnOsal_SemRelease(&ctx->fitFileRingBufSem);

				// Only continue if we were actually able to read something from the buffer
				if(retval != GSN_SUCCESS)
				{
					retval = GSN_BUFFER_OVERFLOW; // A failure to read from the ring buffer indicates a buffer somewhere has overflowed.
					goto HandleFitDataEndExit;
				}

				// Indicate we have read a chunk
				chunksToRead--;

				// If we succeeded to read from the buffer, increment length to the size of the last send
				len += size;
			}

			// Update CRC
			ctx->currentGcsFitFileCrc = CRC_UpdateCRC16(ctx->currentGcsFitFileCrc, (volatile void*)buf, len);

			// If we read in data from one of the last two steps
			if(len > 0)
			{
				UINT32 out_len = 0;
				INT8* encodedData;

				// Base64 encode the data
				encodedData = (INT8*)base64_encode(buf, len, &out_len);

				// If encoding succeded
				if(out_len > 0)
				{
					// URL-encode the base64 data
					Url_Base64Replace(encodedData, &out_len);

					// Send the url-encoded data
					retval = GCS_ConnSendAtomic(encodedData, out_len, 0x01, 0x00, 0x00, ctx);

					// Free the buffer
					gsn_free(encodedData);

					if(retval != GSN_SUCCESS)
					{
						// If CSA fails, we need to set the management state
						ctx->gcs_state = GCS_MGMT_STATE_CLOSED;

						// Record failure in diagnostics
						if(ctx->diagOpen)
						{
							VivohubDiag_RecordGcsFailureEvent(ctx->vivohubDbgCtxPtr, VIVOHUB_DIAG_GCS_SEND_FAIL);
							VivohubDiag_RecordGcsSessionEnd(ctx->vivohubDbgCtxPtr, VS_RESULT_MISC_HTTP_OR_SSL);
							ctx->diagOpen = FALSE;
						}

						goto HandleFitDataEndExit; // Exit if HTTP transfer failed
					}
				}
				else // Encoding failed. Indicates a malloc has failed
				{
					retval = APP_MALLOC_FAILURE;
					goto HandleFitDataEndExit;
				}
			}
		} // end of sending remaining fit file data



		// Send footer
		{
			INT8 sendBuf[32];
			UINT32 offset = 0;

			do
			{
				UINT32 len = 32;

				GCS_GetFitFileJsonFooter(sendBuf, &len, offset, ctx);

				offset += len;

				if(len > 0)
				{
					retval = GCS_ConnSendAtomic(sendBuf, len, 0x01, 0x00, 0x00, ctx); // send data

					// Check send result
					if(retval != GSN_SUCCESS)
					{
						// If CSA fails, we need to set the management state to 'closed'
						ctx->gcs_state = GCS_MGMT_STATE_CLOSED;

						// Record failure in diagnostics
						if(ctx->diagOpen)
						{
							VivohubDiag_RecordGcsFailureEvent(ctx->vivohubDbgCtxPtr, VIVOHUB_DIAG_GCS_SEND_FAIL);

							VivohubDiag_RecordGcsSessionEnd(ctx->vivohubDbgCtxPtr, VS_RESULT_MISC_HTTP_OR_SSL);

							ctx->diagOpen = FALSE;
						}

						goto HandleFitDataEndExit;
					}
				}
				else // We are done sending the footer, break.
				{
					break;
				}

			} while (1);
		} // end of footer


		// Fit file is done being transferred. End the http transfer and collect the result.
		{
			UINT32 rxBufLen = FIT_FILE_RX_BUF_SIZE - 1; // minus one for full-buffer null terminator

			GSN_ASSERT(0 != (rxBuf = gsn_malloc(FIT_FILE_RX_BUF_SIZE)));

			// Send no data, but indicate we are done sending data, and collect the response.
			retval = GCS_ConnSendAtomic(0x00, 0x00, 0x00, rxBuf, &rxBufLen, ctx);

			if(retval != GSN_SUCCESS)
			{
				// If CSA fails, we need to close the management state
				ctx->gcs_state = GCS_MGMT_STATE_CLOSED;

				// Record failure in diagnostics
				if(ctx->diagOpen)
				{
					VivohubDiag_RecordGcsFailureEvent(ctx->vivohubDbgCtxPtr, VIVOHUB_DIAG_GCS_SEND_FAIL);

					VivohubDiag_RecordGcsSessionEnd(ctx->vivohubDbgCtxPtr, VS_RESULT_MISC_HTTP_OR_SSL);

					ctx->diagOpen = FALSE;
				}

				goto HandleFitDataEndExit;
			}

			// Add a null terminator for processing
			rxBuf[rxBufLen] = 0x00;
		} // end of http transfer / response

		// Record any CRC Error
		if(ctx->currentGcsFitFileCrc != 0)
		{
			// Record CRC failure
			if(ctx->diagOpen)
				VivohubDiag_RecordGcsFailureEvent(ctx->vivohubDbgCtxPtr, VIVOHUB_DIAG_GCS_HFDE_CRC_FAIL);
		}

		// Process response

		// Check status line
		if(strncmp(rxBuf, "200", 3) == 0)
		{
			PARSE_JSON_CTX_T j_ctx;
			INT8* response;

			// Search for the response
			response = strchr(rxBuf, '\n') + 1;

			// Validate JSON from GCS
			retval = GCS_ValidateJsonResponse(response, &j_ctx);

			// Check result
			if(retval != GSN_SUCCESS)
			{
				printf("Validation failure: %s\r\n", response);

				// Record failure in diagnostics
				if(ctx->diagOpen)
				{
					VivohubDiag_RecordGcsFailureEvent(ctx->vivohubDbgCtxPtr, VIVOHUB_DIAG_GCS_INVALID_RESPONSE);
					VivohubDiag_RecordGcsSessionEnd(ctx->vivohubDbgCtxPtr, VS_RESULT_INVALID_JSON_IN_RESP);
					ctx->diagOpen = FALSE;
				}

				goto HandleFitDataEndExit;
			}

			// Process JSON from GCS
			retval = GCS_ParseJsonResponse(response, NULL, &j_ctx);

			// Check result
			if(retval != GSN_SUCCESS)
			{
				printf("Process Response Failure: %.150s\r\n", response);

				// Record failure in diagnostics
				if(ctx->diagOpen)
				{
					VivohubDiag_RecordGcsFailureEvent(ctx->vivohubDbgCtxPtr, VIVOHUB_DIAG_GCS_INVALID_RESPONSE);
					VivohubDiag_RecordGcsSessionEnd(ctx->vivohubDbgCtxPtr, VS_RESULT_INVALID_JSON_IN_RESP);
					ctx->diagOpen = FALSE;
				}

				goto HandleFitDataEndExit;
			}
			else // indicates a fully successful fit file transfer.
			{
				printf("GCS Transfer completed successfully ");

				if(ctx->diagOpen)
					VivohubDiag_RecordGcsSessionEnd(ctx->vivohubDbgCtxPtr, VS_RESULT_SUCCESS);
				ctx->diagOpen = FALSE;

				// Check CRC
				if(ctx->currentGcsFitFileCrc == 0)
				{
					printf("and CRC passed\r\n");
				}
				else // CRC check failed. Buffer has become de-sync'd or ANT module is only sending a partial file due to an error on that end
				{
					printf("but CRC failed: 0x%04X\r\n", ctx->currentGcsFitFileCrc);
				}
			}
		} // If 200 OK
		else if(strncmp(rxBuf, "401", 3) == 0)	// Continues checking status line
		{
			// If a 401 is the response, it indicates OAuth has failed. We should set OAuth to retry.
			AppDbg_Printf("GCS Transfer Failed: OAuth Credentials Rejected\r\nRefreshing Credentials\r\n");

			if(ctx->diagOpen)
				VivohubDiag_RecordGcsSessionEnd(ctx->vivohubDbgCtxPtr, VS_RESULT_OAUTH);
			ctx->diagOpen = FALSE;

			// Stop timer and start a much shorter one. If the timer is not currently running, we don't want to shift the time,
			// as the way the OAuth state machine works, if the timer isn't running, there is a message pending waiting to be processed.
			if(GSN_SUCCESS == OAUTH_STOP_TIMER(ctx))
				OAUTH_START_TIMER(ctx, 1);

			retval = APP_INVALID_INCOMING_RESPONSE;
		}
		else	// unhandled http response code
		{
			if(ctx->diagOpen)
				VivohubDiag_RecordGcsSessionEnd(ctx->vivohubDbgCtxPtr, VS_RESULT_MISC_HTTP_OR_SSL);
			ctx->diagOpen = FALSE;

			AppDbg_Printf("GCS Tranfer Failed: Unspecified\r\n");
			AppDbg_BufPrintf((UINT8*)rxBuf, strlen(rxBuf));
			AppDbg_BufPrintf("\r\n", 2);

			retval = APP_INVALID_INCOMING_RESPONSE;
		} // End of status line checks
	} // end of 'if fit file in progress'

HandleFitDataEndExit:

	if(rxBuf)
		gsn_free(rxBuf);

	// Register diagnostics end.
	if(ctx->diagOpen)
	{
		VivohubDiag_RecordGcsSessionEnd(ctx->vivohubDbgCtxPtr, VS_RESULT_OTHER);
		ctx->diagOpen = FALSE;
	}

	// If we have not read enough chunks from the buffer to maintain sync, do so now.
	while(chunksToRead > 0)
	{
		UINT8 buf[GCS_FIT_FILE_CHUNK_SIZE];

		GsnOsal_SemAcquire(&ctx->fitFileRingBufSem, GSN_OSAL_WAIT_FOREVER);

		NpeRingBuf_Get(&ctx->fitFileRingBuf, buf);

		GsnOsal_SemRelease(&ctx->fitFileRingBufSem);

		chunksToRead--;
		ctx->fitFileChunksUntilSend++;
	}

	// Assert that we are still in sync. If this value is off, it indicates something has desync'd.
	APP_ASSERT(ctx->fitFileChunksUntilSend == GCS_MIN_CHUNKS_PER_SEND
			|| ctx->fitFileChunksUntilSend == GCS_MIN_CHUNKS_PER_SEND + 1, ctx->fitFileChunksUntilSend);

	// Reset tracking variables.
	ctx->fitFileSendInProgress = FALSE;
	ctx->fitFileChunksUntilSend = GCS_MIN_CHUNKS_PER_SEND;

	return retval;
}

PRIVATE GSN_STATUS
GCS_DownloadDeviceInfo(GCS_CONN_CTX_T* ctx)
{
    GSN_STATUS  retval;
    UINT16      downloadOffset;
    INT8*		rxBuf = 0;

    retval = GCS_ConnOpen(ctx);
    if( GSN_SUCCESS != retval ) {
        return retval;
    }

    for( downloadOffset = 0; downloadOffset < DEVICE_TABLE_SIZE; downloadOffset += NUM_RECORDS)
    {
        // Send the header
        UINT32 offset = 0;
        INT8        downloadOffsetStr[8];
        INT8		sendStr[JSON_TX_BUF_SIZE] = {0};		// Length is used again later to copy into buffer

        snprintf(downloadOffsetStr, 7, "%d", downloadOffset);

        do
        {
            UINT32 i = JSON_TX_BUF_SIZE;

            // Get Download Device Info JSON request
            GCS_GetDeviceInfoJson(sendStr, &i, offset, downloadOffsetStr, NUM_RECORDS_STR,
            		RECORD_LIMIT_STR,
                     ctx);

            // Increment offset by the number of bytes we just received
            offset += i;

            // Send all data received from Json Header
            if(i > 0)
            {
                if(GSN_SUCCESS != (retval = GCS_ConnSendAtomic(sendStr, i, 0x01, 0x00, 0x00, ctx))) // Don't register a receive buffer yet.
                {
                    goto DownloadDevTableExit;
                }
            }
            else
            {
            	UINT32 len = DEVICE_TABLE_RX_BUF_SIZE - 1;	// Minus one for null terminator

            	// Allocate receive buffer, if the loop has not yet allocated it.
            	if(rxBuf == 0)
            		GSN_ASSERT(0 != (rxBuf = gsn_malloc(DEVICE_TABLE_RX_BUF_SIZE)));

            	if(GSN_SUCCESS != (retval = GCS_ConnSendAtomic(0x00, 0x00, 0x00, rxBuf, &len, ctx)))
            	{
            		goto DownloadDevTableExit;
            	}

            	rxBuf[len] = 0x00;

                break; // Break when we have no more header to send.
            }
        }
        while(1);

        // Final send

        // If this is the response to the first Download Device Info request and the response contains valid data,
        //   then we clear out the existing device table.
        // We verify the response first to make sure we don't trash the table, then find out we have an error response.
        // If the response is not valid, we continue using the existing table.
        if(strncmp(rxBuf, "200", 3) == 0)	// If we get a '200 OK' response
        {
            PARSE_JSON_CTX_T j_ctx;		// json context. 27 bytes long, so we allocate it down here to save stack during send/rx functions
        	// For some reason, the receive buffer always contains the status line as the first line of the buffer. We skip it here.
        	INT8* response = strchr(rxBuf, '\n');

			if((retval = GCS_ValidateJsonResponse(response, &j_ctx)) == GSN_SUCCESS)
			{
				UINT32 numCcfs;

				if( downloadOffset == 0 )
				{
					GCS_ClearDeviceTable();
				}

				if(GSN_SUCCESS == GCS_ParseJsonResponse(response, &numCcfs, &j_ctx))
				{
					ctx->devtable_avail = TRUE;

					// If there are fewer CCF's in the response than we requested, don't request more
					if(numCcfs < NUM_RECORDS)
						break;

					// Kick the sysqual before we continue.
					GsnSq_TaskMonitorStop(ctx->sysQualId);
					GsnSq_TaskMonitorStart(ctx->sysQualId, GCS_UPDATE_DEVTABLE);
				}
			}
			else // Invalid JSON
			{
				// debug. remove
				AppDbg_BufPrintf((UINT8*)response, strlen(response));

				goto DownloadDevTableExit;	// Bail
			}
        }
        else if(strncmp(rxBuf, "401", 3) == 0) // 401 unauthorized is an oauth failure
        {
			// If a 401 is the response, it indicates OAuth has failed. We should set OAuth to retry.
			AppDbg_Printf("Devtable Download Failed: OAuth Credentials Rejected\r\nRefreshing Credentials\r\n");

			// Stop timer and start a much shorter one. If the timer is not currently running, we don't want to shift the time,
			// as the way the OAuth state machine works, if the timer isn't running, there is a message pending waiting to be processed.
			if(GSN_SUCCESS == OAUTH_STOP_TIMER(ctx))
				OAUTH_START_TIMER(ctx, 1);

			retval = APP_INVALID_INCOMING_RESPONSE;

        	goto DownloadDevTableExit;
        }
        else
        {
        	AppDbg_Printf("Devtable Download Failed.\r\n");
        	AppDbg_BufPrintf((UINT8*)rxBuf, strlen(rxBuf));

        	retval = APP_INVALID_INCOMING_RESPONSE;

        	goto DownloadDevTableExit;
        }
    }

    retval = GSN_SUCCESS;

DownloadDevTableExit:

	if(rxBuf)
		gsn_free(rxBuf);

    GCS_ConnClose(ctx);
    return retval;
}

PRIVATE GSN_STATUS
GCS_RegisterWellnessBridge(GCS_CONN_CTX_T* ctx)
{
	GSN_STATUS ret;
	INT8* rxBuf = 0;

	// First, we open the connection to GCS, it if isn't already.
	ret = GCS_ConnOpen(ctx);
	if(ret != GSN_SUCCESS)
		goto EXIT;

	// Send outoing message
	{
		INT8 buf[JSON_TX_BUF_SIZE];
		UINT32 offset = 0;
		UINT32 len;

		// We send the outgoing request in chunks to save stack. This loop individually sends each chunk, then collects the response when
		// we have no more chunks to send.
		do
		{
			// Set max number of bytes to get at a time
			len = sizeof(buf);

			// Get JSON
			GCS_GetRegisterBridgeJson(buf, &len, offset, ctx);

			// increment total offset in json string
			offset += len;

			// Send JSON
			if(len > 0)
			{
				ret = GCS_ConnSendAtomic(buf, len, TRUE, NULL, 0, ctx);

				if(ret != GSN_SUCCESS)
					goto EXIT;
			}
			else // Indicates transfer has completed
			{
				UINT32 rxlen = REG_BRIDGE_RX_BUF_SIZE - 1;

				// Allocate receive buffer
				GSN_ASSERT(0 != (rxBuf = gsn_malloc(REG_BRIDGE_RX_BUF_SIZE)));

				ret = GCS_ConnSendAtomic(NULL, 0, FALSE, rxBuf, &rxlen, ctx);

				if(ret != GSN_SUCCESS)
					goto EXIT;

				rxBuf[rxlen] = 0x00;

				break; // exit loop and parse response
			}
		} while (1);
	}

	// Begin parsing the response
	if(strncmp(rxBuf, "200", 3) == 0)
	{
		PARSE_JSON_CTX_T j_ctx;

		INT8* response = strchr(rxBuf, '\n');

		ret = GCS_ValidateJsonResponse(response, &j_ctx);

		// If the response was valid
		if(ret == GSN_SUCCESS)
		{
			// return success
			goto EXIT;
		}
		else
		{
			// debug. remove
			AppDbg_BufPrintf((UINT8*)rxBuf, strlen(response));

			// Return an error
			ret = APP_INVALID_INCOMING_RESPONSE;
			goto EXIT;
		}
	}
	else if(strncmp(rxBuf, "401", 3) == 0)
	{
		// Indicates we are unauthorized, which means our oauth credentials are invalid. Restart OAuth.
		AppDbg_Printf("Devtable Download Failed: OAuth Credentials Rejected\r\nRefreshing Credentials\r\n");

		// Stop timer and start a much shorter one. If the timer is not currently running, we don't want to shift the time,
		// as the way the OAuth state machine works, if the timer isn't running, there is a message pending waiting to be processed.
		if(GSN_SUCCESS == OAUTH_STOP_TIMER(ctx))
			OAUTH_START_TIMER(ctx, 1);

		ret = APP_INVALID_INCOMING_RESPONSE;
		goto EXIT;
	}
	else
	{
		ret = APP_INVALID_INCOMING_RESPONSE;
	}

EXIT:
	if(rxBuf)
		gsn_free(rxBuf);

	return ret;
}

PRIVATE GSN_STATUS
GCS_UlDiagnosticsData(GCS_CONN_CTX_T* ctx)
{
	GSN_STATUS ret;
	INT8* rxBuf = 0;
	INT8 txBuf[DIAG_TX_BUF_SIZE];	// larger because diagnostics needs a larger tx buffer
	UINT32 len;
	UINT32 offset = 0;

	// First, we open the connection to GCS
	ret = GCS_ConnOpen(ctx);
	if(ret != GSN_SUCCESS)
		goto EXIT;

	// Next, we send the JSON header
	{

		do
		{
			len = sizeof(txBuf);

			GCS_GetUlDataJsonHeader(txBuf, &len, offset, ctx);

			offset += len;

			if(len > 0)
			{
				ret = GCS_ConnSendAtomic(txBuf, len, TRUE, NULL, NULL, ctx);

				if(ret != GSN_SUCCESS)
					goto EXIT;
			}
		} while(len > 0);
	}

	// Send base-64 encoded diag file data
	{
		UINT32 b64Len;
		INT8* b64Buf;
		DIAG_FILE_T fileCtx = {0};
		UINT8 leftovers[3];
		UINT8 leftoverIdx = 0;

		do
		{
			b64Len = 0;
			len = sizeof(txBuf) - 3; // Minus three for leftovers

			// Get diagnostics file
			DiagFile_Read(txBuf, &len, ctx->diagCtx, &fileCtx);

			// Base-64 encode the buffer
			if(len > 0)
			{
				// base64 strings have to be encoded in multiples of 3. we store excess in the 'leftovers' buffer. Add previous leftovers here
				if(leftoverIdx)
				{
					memmove(txBuf + leftoverIdx, txBuf, len);
					memcpy(txBuf, leftovers, leftoverIdx);
					len += leftoverIdx;
					leftoverIdx = 0;
				}

				// Copy any leftovers into the leftover buffer
				if(len % 3)
				{
					memcpy(leftovers, &txBuf[len - (len % 3)], len % 3);
					leftoverIdx = len % 3;
					len -= len % 3;
				}

				APP_ASSERT2(len % 3 == 0, leftoverIdx, len);

				b64Buf = (INT8*)base64_encode((UINT8*)txBuf, len, &b64Len);
			}
			else	// transfer is done. Send leftovers
			{
				// If we have leftovers
				if(leftoverIdx > 0)
				{
					b64Buf = (INT8*)base64_encode((UINT8*)leftovers, leftoverIdx, &b64Len);
				}
			}

			// If base-64 encoding succeded, it means we have data to send
			if(b64Len > 0)
			{
				// Url-encode and swap necessary characters in the encoding
				Url_Base64Replace(b64Buf, &b64Len);

				// Send data
				ret = GCS_ConnSendAtomic(b64Buf, b64Len, TRUE, NULL, NULL, ctx);

				// Free the buffer
				gsn_free(b64Buf);

				// Check send result
				if(ret != GSN_SUCCESS)
				{
					// If we fail, we have to end the read before we can exit.
					Diag_EndRead(ctx->diagCtx, FALSE);

					goto EXIT;
				}

				// If the send succeeded, kick the sysqual. This transfer can take a loong time, and so long as it is still running, we don't want the GCS sysqual to catch it.
				GsnSq_TaskMonitorStop(ctx->sysQualId);
				GsnSq_TaskMonitorStart(ctx->sysQualId, GCS_SEND_DIAGNOSTICS);
			}

			// Check to see if we're done and exit.
			if(len == 0)
				break;

		} while(TRUE);
	}

	// Send the json footer
	{
		offset = 0;

		do
		{
			len = sizeof(txBuf);

			GCS_GetUlDataJsonFooter(txBuf, &len, offset);

			offset += len;

			if(len > 0)
			{
				ret = GCS_ConnSendAtomic(txBuf, len, TRUE, NULL, NULL, ctx);

				if(ret != GSN_SUCCESS)
				{
					// End reading before we exit
					Diag_EndRead(ctx->diagCtx, FALSE);

					goto EXIT;
				}
			}

		} while(len > 0);
	}

	// Collect a response
	{
		rxBuf = gsn_malloc(UL_DATA_RX_BUF_SIZE);

		len = UL_DATA_RX_BUF_SIZE;

		ret = GCS_ConnSendAtomic(NULL, 0, FALSE, rxBuf, &len, ctx);

		if(ret != GSN_SUCCESS)
		{
			// Close reading before we exit in error
			Diag_EndRead(ctx->diagCtx, FALSE);

			goto EXIT;
		}

		rxBuf[len] = 0x00;
	}

	// Parse response
	if(strncmp(rxBuf, "401", 3) == 0)	// If we are unauthorized, then oauth is failing and we must redo credentials.
	{
		// Stop timer and start a much shorter one. If the timer is not currently running, we don't want to shift the time,
		// as the way the OAuth state machine works, if the timer isn't running, there is a message pending waiting to be processed.
		if(GSN_SUCCESS == OAUTH_STOP_TIMER(ctx))
			OAUTH_START_TIMER(ctx, 1);

		ret = APP_INVALID_INCOMING_RESPONSE;

		// Close reading before we exit
		Diag_EndRead(ctx->diagCtx, FALSE);

		goto EXIT;
	}
	else if(strncmp(rxBuf, "200", 3) == 0)
	{
		PARSE_JSON_CTX_T jCtx = {0};

		// Validate JSON
		ret = GCS_ValidateJsonResponse(strchr(rxBuf, '\n') + 1, &jCtx);

		// If validation succeded, then we can wipe the diagnostics as we exit with a success
		if(ret == GSN_SUCCESS)
		{
			Diag_EndRead(ctx->diagCtx, TRUE);

			goto EXIT;
		}
		else	// If validation failed, we got an invalid incoming response. return that. Do not wipe diagnostics.
		{
			Diag_EndRead(ctx->diagCtx, FALSE);

			ret = APP_INVALID_INCOMING_RESPONSE;

			goto EXIT;
		}
	}
	else // unrecognized response.
	{
		Diag_EndRead(ctx->diagCtx, FALSE);

		ret = APP_INVALID_INCOMING_RESPONSE;

		goto EXIT;
	}

EXIT:
	if(rxBuf)
		gsn_free(rxBuf);

	return ret;
}

// Indicates a TCP connection has been open long enough without a device connected, and should be closed.
PRIVATE VOID
GCS_ConnectionExpireCB( VOID* context, GSN_SOFT_TMR_HANDLE_T  timerHandle )
{
	GCS_CONN_CTX_T*     ctx = context;

	GCS_SendMessage(GCS_CONN_CLOSE, 0, ctx);
}

PRIVATE VOID
GCS_OAuthTimerCB( VOID* context, GSN_SOFT_TMR_HANDLE_T  timerHandle )
{
    GCS_CONN_CTX_T*     ctx = context;

    // Acquire access to the external state
    if(GSN_SUCCESS == GsnOsal_MutexGet(&ctx->ext_state_mut, GSN_OSAL_NO_WAIT))
    {
		if(ctx->ext_gcs_state == GCS_MGMT_STATE_CLOSED)
		{
			GCS_SendMessage(GCS_UPDATE_OAUTH, 0, ctx);
		}
		else
		{
			if(ctx->ext_gcs_state == GCS_MGMT_STATE_READY)
				ctx->ext_gcs_state = GCS_MGMT_STATE_READY_PENDING;

			ctx->ext_gcs_oauth_pending = TRUE;
		}

		GsnOsal_MutexPut(&ctx->ext_state_mut);
    }
    else	// Failed to acquire the mutex. Don't want to wait, since this is within the soft timer context.
    {
    	// We re-start the timer
    	OAUTH_START_TIMER(ctx, GCS_OAUTH_RETRY_TIMEOUT);
    }
}

PRIVATE VOID
GCS_DevTableTimerCB( VOID* context, GSN_SOFT_TMR_HANDLE_T  timerHandle )
{
    GCS_CONN_CTX_T*     ctx = context;
    GSN_STATUS ret;

    ret = GsnOsal_MutexGet(&ctx->ext_state_mut, GSN_OSAL_NO_WAIT);

    if(ret == GSN_SUCCESS)
    {
		if(ctx->ext_gcs_state == GCS_MGMT_STATE_CLOSED)
		{	// If we are closed
			// Just put a message in the queue. We're not in the middle of a transfer.
			GCS_SendMessage(GCS_UPDATE_DEVTABLE, 0, ctx);
		}
		else	// If the outside functions are currently in the process of sending a message
		{
			// Set the state to indicate that the dev table is pending
			if(ctx->ext_gcs_state == GCS_MGMT_STATE_READY)
				ctx->ext_gcs_state = GCS_MGMT_STATE_READY_PENDING;

			ctx->ext_gcs_devtable_pending = TRUE;
		}

		GsnOsal_MutexPut(&ctx->ext_state_mut);
    }
    else	// Failed to acquire the mutex. Don't want to wait, since this is within the soft timer context.
    {
    	// We re-start the timer
    	DEVTABLE_START_TIMER(ctx, GCS_DEVTABLE_RETRY_TIMEOUT);
    }
}

PRIVATE VOID
GCS_UlDiagTimerCB( VOID* context, GSN_SOFT_TMR_HANDLE_T  timerHandle )
{
	GCS_CONN_CTX_T* ctx = (GCS_CONN_CTX_T*)context;
	GSN_STATUS ret;

	ret = GsnOsal_MutexGet(&ctx->ext_state_mut, GSN_OSAL_NO_WAIT);

	if(ret == GSN_SUCCESS)
	{
		if(ctx->ext_gcs_state == GCS_MGMT_STATE_CLOSED)
		{
			GCS_SendMessage(GCS_SEND_DIAGNOSTICS, 0, ctx);
		}
		else
		{
			if(ctx->ext_gcs_state == GCS_MGMT_STATE_READY)
				ctx->ext_gcs_state = GCS_MGMT_STATE_READY_PENDING;

			ctx->ext_gcs_diag_ul_pending = TRUE;
		}

		GsnOsal_MutexPut(&ctx->ext_state_mut);
	}
	// else, just wait another hour to send diagnostics. It's no biggie if we miss an upload here and there.
}

PRIVATE VOID
GCS_SendMessage(GCS_MGMT_MESSAGE_T msg, UINT8 size, GCS_CONN_CTX_T* ctx)
{
	UINT32 message = 0;
	GSN_STATUS ret;

	((GCS_MGMT_MSG*)&message)->msg = msg;
	((GCS_MGMT_MSG*)&message)->bufSize = size;

	ret = GsnOsal_QueuePut(&ctx->gcsMsgQueue, (UINT8*)&message);

	if(ret != GSN_SUCCESS)
	{
		APP_ASSERT(FALSE, msg);
	}
}
