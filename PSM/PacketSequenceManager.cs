﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WTestUtility
{
    // Handles out-of-order packets
    class PacketSequenceManager
    {
        // 'contents' is the contents of the packet which should be processed, 'deltaPacketCount' is the change in EXPECTED packets. If a packet is missed, this 
        // count is incremented by the number of missed packets. If we later receive a packet that was missed earlier, the count isn't incremented as it was 
        // already incremented for this missed packet.
        public delegate void HandlePacket(byte[] Contents, int deltaPacketCount, object reference);

        // Array of recently received packets of variable depth.
        List<Packet> queue;

        // Next expected sequence number
        byte sequence;

        // For handling the packets.
        HandlePacket handle;
        object reference;

        // depth
        uint depth;

        public PacketSequenceManager(uint depth, byte firstSequence, byte[] firstPkt, HandlePacket method, object reference)
        {
            if (depth <= 1)
                throw new Exception("Invalid Depth");

            handle = method;
            sequence = (byte)(firstSequence + 1);

            queue = new List<Packet>();

            this.depth = depth;
            this.reference = reference;

            // immediately call handle
            handle(firstPkt, 1, this.reference);
        }

        public void Process(byte[] packet, byte sequence)
        {
            // If sequence number matches expected sequence number
            if (sequence == this.sequence)
            {
                // increment local sequence number
                this.sequence++;

                // Invoke method to process data
                handle(packet, 1, reference);
            }
            else  // if packet does not match sequence number
            {
                // if we have found a spot for it between two values in the array..
                bool found = false; 
                
                // pre-emptively create a packet
                Packet p;
                p.Sequence = sequence;
                p.Contents = packet;
                p.Initialized = true;
                
                // Insert into corresponding place in array
                if (queue.Count > 0)
                {
                    // If this sequence fits before the first index
                    for (byte i = (byte)(this.sequence + 1); i != queue[0].Sequence; i++)
                    {
                        if(i == sequence)
                        {
                            queue.Insert(0, p);
                            found = true;
                            break;
                        }
                    }

                    // Scan and check to see if the sequence fits between two array values
                    if (!found)
                    {
                        for (int queueIndex = 0; queueIndex < queue.Count - 1; queueIndex++)
                        {
                            // check to see if the current packet sequence lies between two packets in the queue
                            for (byte i = queue[queueIndex].Sequence; i != queue[queueIndex + 1].Sequence; i++)
                            {
                                // if the current packet's location is found
                                if (i == sequence)
                                {
                                    queue.Insert(queueIndex + 1, p);

                                    found = true;

                                    break;
                                }
                            }

                            if (found)
                                break;
                        }
                    }
                }

                // if we did not find a spot for this value in the queue.
                if(!found)
                {
                    // Add it to the end of the queue
                    queue.Add(p);
                }

                // Check queue to make sure it is within the tolerance depth
                int difference = queue[queue.Count - 1].Sequence - this.sequence;
                if (difference < 0) // roll in case sequence numbers have rolled over.
                    difference += 256;

                // While the difference is above max depth
                while(queue.Count > 0 && difference > depth)
                {
                    // Push out the oldest sequence numbers

                    // Calculate difference between sequence and first item
                    int space = queue[0].Sequence - this.sequence;
                    if (space < 0)
                        space += 256;

                    // Increment by one to account for item missed by pre-incremented sequence number
                    space++;

                    // Handle this entry in the queue
                    handle(queue[0].Contents, space, reference);

                    // Remove from queue
                    queue.RemoveAt(0);

                    // Add to sequence
                    this.sequence += (byte)space;

                    // Subtract from difference
                    difference -= space;
                }
            }

            // Next, check to see if the incrementing of the sequence number allows us to flush more values out of the queue.
            while (queue.Count > 0 && this.sequence == queue[0].Sequence)
            {
                this.sequence++;

                handle(queue[0].Contents, 1, reference);

                queue.RemoveAt(0);
            }
        }

        // Push all packets out of handle function
        public void Flush()
        {
            while(queue.Count > 0)
            {
                int difference = queue[0].Sequence - this.sequence;
                if(difference < 0)
                    difference += 256;

                difference++;

                handle(queue[0].Contents, difference, reference);

                this.sequence += (byte)difference;

                queue.RemoveAt(0);
            }
        }

        private struct Packet
        {
            public bool Initialized;
            public byte Sequence;
            public byte[] Contents;
        }
    }
}
