/*
 * oauth.h
 *
 * Contains a generic OAuth Implementation that can be used in a two-legged authentication scheme.
 *
 *  Created on: Dec 16, 2013
 *      Author: andrewk
 */

#ifndef OAUTH_H_
#define OAUTH_H_

#include "gsn_includes.h"

#define OAUTH_VERSION "1.0"

#define OAUTH_MAX_NUM_PARAMS 10
#define OAUTH_MAX_PARAM_SIZE 256 // Maximum number of characters in an oauth field.

typedef enum OAUTH_SIG_METHOD
{
	OAUTH_SIG_METHOD_HMAC_SHA1
	//,OAUTH_SIG_METHOD_RSA_SHA1		// Not implemented
} OAUTH_SIG_METHOD_T;

/**
 * Context structure for an OAuth Connection
 */
typedef struct OAUTH_CTX
{
	INT8* 	consumerKey;
	INT8* 	consumerSecret;
	OAUTH_SIG_METHOD_T sigMethod;
	INT8*   tokenKey;
	UINT32  tokenKeyMaxLen;
	INT8*   tokenSecret;
	UINT32  tokenSecretMaxLen;
} OAUTH_CTX_T;

/**
 * Structure which stores the string value for the parameters for an oauth request.
 * Used because we need to be able to sort them to generate a signature
 */
typedef struct OAUTH_REQ
{
	INT8* paramNames[OAUTH_MAX_NUM_PARAMS];
	INT8* paramValues[OAUTH_MAX_NUM_PARAMS];
	UINT8  numParams;
} OAUTH_REQ_T;

/**
 * Initializes an OAuth context structure.
 * @param[in] consumerKey Consumer Key to be used in communication.
 * @param[in] consumerSecret Consumer Secret to be used in communication.
 * @param[out] oauth_ctx Context structure to be populated
 */
PUBLIC VOID
OAuth_InitCtx(INT8* consumerKey, INT8* consumerSecret, INT8* tokenKeyBuffer, UINT32 tokenKeyBufSize, INT8* tokenSecretBuffer, UINT32 tokenSecretBufSize, OAUTH_CTX_T* oauth_ctx);

/**
 * Generates a 'temporary token request' header to be placed under the Authorization: header in HTTP.
 * @param[out] header A properly-sized string buffer to write the oauth information to.
 * @param[in] requestURL The full URL, including http://, of the location where we will make the request. Used in the signature.
 * @param[in] oauth_ctx Context structure of the current oauth connection
 * @note URL cannot contain GET parameters or the signature will be invalid
 * @note The HTTP request cannot contain an entity-body AND a 'application/x-www-form-urlencoded' content-type
 */
PUBLIC VOID
OAuth_GenRequestHeader(INT8* header, INT8* requestURL, OAUTH_CTX_T* oauth_ctx);

/**
 * Generates a header, with tokens, for requesting 1) access or 2) a permanent token from a temporary token
 * @param[out] header A properly-sized string buffer to write the oauth header to
 * @param[in] requestURL The full URL, including http:// of the loc to make the request. Used in a sig.
 * @param[in] oauth_ctx Context structure of the current oauth connection
 * @note URL cannot contain GET parameters or the signature will be invalid
 * @note The HTTP request cannot contain an entity-body AND a 'application/x-www-form-urlencoded' content-type
 */
PUBLIC VOID
OAuth_GenAccessHeader(INT8* header, INT8* requestURL, OAUTH_CTX_T* oauth_ctx);

/**
 * Parses an http response body for a token and token secret. Will fail if parameters are not encoded correctly.
 * Populates the memory pointed to by tokenKey and tokenSecret with new data.
 * @param[in] response null-terminated Response to parse
 * @param	oauth_ctx	OAuth context structure. Will be modified with the token
 */
PUBLIC GSN_STATUS
OAuth_ParseResponseBody(INT8* response, OAUTH_CTX_T* oauth_ctx);

#endif /* OAUTH_H_ */
