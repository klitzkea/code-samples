/*
 * oauth.c
 *
 *  Created on: Dec 16, 2013
 *      Author: andrewk
 */

#include "gsn_includes.h"
#include "oauth\oauth.h"
#include "encoding\url.h"
#include "supplicant\crypto\sha1.h"
#include "supplicant\utils\base64.h"

// Typedefs

// For parsing
typedef enum
{
	OAUTH_PARSE_STATE_NAME,
	OAUTH_PARSE_STATE_VALUE
} OAUTH_PARSE_STATE_T;

// Initializers

static const INT8* sigMethodStr[] =
{
"HMAC-SHA1"		// OAUTH_SIG_METHOD_HMAC_SHA1
//,"RSA-SHA1"
};

// Private declarations

PRIVATE VOID
OAuth_AddParam(INT8* name, INT8* value, OAUTH_REQ_T* oauth_req);

PRIVATE INT8*
OAuth_AddSignature(INT8* requestURL, OAUTH_REQ_T* oauth_req, const OAUTH_CTX_T* oauth_ctx);

PRIVATE INT8*
OAuth_GetTimestamp( VOID );

PRIVATE INT8*
OAuth_GetNonce( VOID );

PRIVATE VOID
OAuth_GenerateHeaderStr(INT8* header, OAUTH_REQ_T* oauth_req);

PRIVATE VOID
OAuth_SortReq(OAUTH_REQ_T* output, const OAUTH_REQ_T* input);

PRIVATE INT8*
OAuth_HMAC_SHA1(const INT8* source, const OAUTH_CTX_T* oauth_ctx);

PRIVATE GSN_STATUS
OAuth_ProcessResponse(INT8* name, INT8* value, OAUTH_CTX_T* oauth_ctx);

PRIVATE INLINE UINT8
isBefore(const INT8* str1, const INT8* str2);

// Public functions

PUBLIC VOID
OAuth_InitCtx(INT8* consumerKey, INT8* consumerSecret, INT8* tokenKeyBuffer, UINT32 tokenKeyBufSize, INT8* tokenSecretBuffer, UINT32 tokenSecretBufSize, OAUTH_CTX_T* oauth_ctx)
{
	// Erase first
	memset(oauth_ctx, 0x00, sizeof(OAUTH_CTX_T));
	memset(tokenKeyBuffer, 0x00, tokenKeyBufSize);
	memset(tokenSecretBuffer, 0x00, tokenSecretBufSize);

	oauth_ctx->consumerKey = consumerKey;
	oauth_ctx->consumerSecret = consumerSecret;
	oauth_ctx->tokenSecret = tokenSecretBuffer;
	oauth_ctx->tokenSecretMaxLen = tokenSecretBufSize;
	oauth_ctx->tokenKey = tokenKeyBuffer;
	oauth_ctx->tokenKeyMaxLen = tokenKeyBufSize;
	oauth_ctx->sigMethod = OAUTH_SIG_METHOD_HMAC_SHA1; // Hardcode for now. Add to initctx() params if needed.
}

PUBLIC VOID
OAuth_GenRequestHeader(INT8* header, INT8* requestURL, OAUTH_CTX_T* oauth_ctx)
{
	OAUTH_REQ_T* oauth_req;
	INT8* sig;			// For freeing
	INT8* timestamp;	// For freeing
	INT8* nonce; 		// For freeing

	// Allocate variables
	oauth_req = gsn_malloc(sizeof(OAUTH_REQ_T));
	memset(oauth_req, 0x00, sizeof(OAUTH_REQ_T));

	// Reset Token Key, as if we're generating a request header, we shouldn't have a token.
	oauth_ctx->tokenSecret[0] = 0x00;
	oauth_ctx->tokenKey[0] = 0x00;

	// Add consumer key
	OAuth_AddParam("oauth_consumer_key", oauth_ctx->consumerKey, oauth_req);

	// Add Signature method
	OAuth_AddParam("oauth_signature_method", (INT8*)sigMethodStr[oauth_ctx->sigMethod], oauth_req);

	// Add version
	OAuth_AddParam("oauth_version", OAUTH_VERSION, oauth_req);

	// Add empty token as per garmin specs
	OAuth_AddParam("oauth_token", "", oauth_req);

	// Nonce
	nonce = OAuth_GetNonce();
	OAuth_AddParam("oauth_nonce", nonce, oauth_req);

	// Timestamp
	timestamp = OAuth_GetTimestamp();
	OAuth_AddParam("oauth_timestamp", timestamp, oauth_req);

	// Generate and add signature.
	sig = OAuth_AddSignature(requestURL, oauth_req, oauth_ctx);

	// Request structure is now populated. Generating header string.
	OAuth_GenerateHeaderStr(header, oauth_req);

	// Release strings
	gsn_free(oauth_req);
	gsn_free(sig);
	gsn_free(timestamp);
	gsn_free(nonce);
}

PUBLIC VOID
OAuth_GenAccessHeader(INT8* header, INT8* requestURL, OAUTH_CTX_T* oauth_ctx)
{
	OAUTH_REQ_T* oauth_req;
	INT8* sig;			// record it, so we can free it later
	INT8* timestamp;	// record it, so we can free it later
	INT8* nonce; 		// ^^

	// Allocate memory
	oauth_req 	= gsn_malloc(sizeof(OAUTH_REQ_T));
	memset(oauth_req, 0x00, sizeof(OAUTH_REQ_T));

	// Add Consumer Key
	OAuth_AddParam("oauth_consumer_key", oauth_ctx->consumerKey, oauth_req);

	OAuth_AddParam("oauth_signature_method", (INT8*)sigMethodStr[oauth_ctx->sigMethod], oauth_req);

	OAuth_AddParam("oauth_version", OAUTH_VERSION, oauth_req);

	OAuth_AddParam("oauth_token", oauth_ctx->tokenKey, oauth_req);

	nonce = OAuth_GetNonce();
	OAuth_AddParam("oauth_nonce", nonce, oauth_req);

	timestamp = OAuth_GetTimestamp();
	OAuth_AddParam("oauth_timestamp", timestamp, oauth_req);

	sig = OAuth_AddSignature(requestURL, oauth_req, oauth_ctx);

	OAuth_GenerateHeaderStr(header, oauth_req);

	// Release strings
	gsn_free(oauth_req);
	gsn_free(sig);
	gsn_free(timestamp);
	gsn_free(nonce);
}

PUBLIC GSN_STATUS
OAuth_ParseResponseBody(INT8* response, OAUTH_CTX_T* oauth_ctx)
{
	OAUTH_PARSE_STATE_T state = OAUTH_PARSE_STATE_NAME;	// Used to track what the function is currently parsing.
	GSN_STATUS retval = GSN_SUCCESS;
	UINT32 len;
	UINT32 i;
	INT8* temp_str;
	INT8* name_str;
	INT8* value_str;

	// Memory allocation
	value_str = gsn_malloc(OAUTH_MAX_PARAM_SIZE + 1);
	name_str = gsn_malloc(OAUTH_MAX_PARAM_SIZE + 1);
	temp_str = gsn_malloc(OAUTH_MAX_PARAM_SIZE + 1);	// Used to temporarily store a string while parsing

	// Clear strings
	memset(temp_str, 0x00, OAUTH_MAX_PARAM_SIZE + 1);
	memset(name_str, 0x00, OAUTH_MAX_PARAM_SIZE + 1);
	memset(value_str, 0x00, OAUTH_MAX_PARAM_SIZE + 1);

	// Set length
	len = strlen(response);

	// Parsing occurs one character at a time.
	for(i = 0; i < len; i++)
	{
		// Switch where we are currently parsing within the string
		switch(state)
		{
		case OAUTH_PARSE_STATE_NAME           : // Currently parsing name
			// If we are within the name.
			if(strlen(temp_str) < OAUTH_MAX_PARAM_SIZE)
			{
				// If response is an equals sign
				if(response[i] == '=')
				{
					// Copy the name to a more permanent location
					strcpy(name_str, temp_str);

					// Clear temporary string
					memset(temp_str, 0x00, OAUTH_MAX_PARAM_SIZE + 1);

					// Switch State
					state = OAUTH_PARSE_STATE_VALUE;
				}
				else if(!Url_ShouldEncode(response[i], URL_ENCODING_OAUTH)) // If we have a valid url-safe character in the name
				{
					// We add that value to our temporary string
					strncat(temp_str, &response[i], 1);
				}
				else	// Unrecognized character
				{
					// exit
					retval = GSN_INVALID_PARAM;
					goto EXIT;
				}
			}
			else // Overflow, exit with a failure
			{
				retval = GSN_BUFFER_OVERFLOW;
				goto EXIT;
			}
			break;
		case OAUTH_PARSE_STATE_VALUE          :
			if(response[i] == '&' || response[i] == 0x00)
			{
				// Copy to semi-perm string
				strcpy(value_str, temp_str);

				// clear temp string
				memset(temp_str, 0x00, OAUTH_MAX_PARAM_SIZE + 1);

				// Set state
				state = OAUTH_PARSE_STATE_NAME;

				// Add name and value pair
				goto ADD;
			}
			else if(!Url_ShouldEncode(response[i], URL_ENCODING_OAUTH) || response[i] == '%')  // 0-9, a-z, A-Z, and allow percent encoding.
			{
				if(strlen(temp_str) < OAUTH_MAX_PARAM_SIZE)
				{
					strncat(temp_str, &response[i], 1); // Copy the character to the temporary string

					// If we have reached the end of the string
					if(i == len - 1)
					{
						// Copy temporary string into value string.
						strcpy(value_str, temp_str);

						goto ADD;
					}
				}
				else // Buffer Overflow
				{
					retval = GSN_BUFFER_OVERFLOW;
					goto EXIT;
				}
			}
			else // value not recognized
			{
				retval = GSN_INVALID_PARAM;
				goto EXIT;
			}
			break;
		ADD:
			if((retval = OAuth_ProcessResponse(name_str, value_str, oauth_ctx)) != GSN_SUCCESS)
				goto EXIT;

			name_str[0] = 0x00;	// reset name string
			value_str[0] = 0x00; // reset value string
			break;
		default:
			retval = GSN_FAILURE;
			goto EXIT;
		}
	}

	// Deallocates arrays and exits.
EXIT:
	gsn_free(name_str);
	gsn_free(value_str);
	gsn_free(temp_str);
	return retval;
}

// Private functions

PRIVATE VOID
OAuth_AddParam(INT8* name, INT8* value, OAUTH_REQ_T* oauth_req)
{
	// If the structure has room for another parameter
	if(oauth_req->numParams < OAUTH_MAX_NUM_PARAMS - 1)
	{
		// Store the name
		oauth_req->paramNames[oauth_req->numParams] = name;

		// Store the value
		oauth_req->paramValues[oauth_req->numParams] = value;

		// Increment the number of values
		oauth_req->numParams++;
	}
}

// Returns a pointer to the signature, which should be freed by the user later. Automatically signs the request structure though.
PRIVATE INT8*
OAuth_AddSignature(INT8* requestURL, OAUTH_REQ_T* oauth_req, const OAUTH_CTX_T* oauth_ctx)
{
	INT8 i;
	OAUTH_REQ_T* ord_req;
	INT8* temp_buffer;
	INT8* signature_dest;

	// Allocate memory
	ord_req 		= gsn_malloc(sizeof(OAUTH_REQ_T));
	temp_buffer 	= gsn_malloc(512);


	// Sort outgoing request alphabetically
	OAuth_SortReq(ord_req, oauth_req);

	// Begin generating signature base string

	// We always use a POST http method
	strcpy(temp_buffer, "POST&");

	// Copy URL
	Url_strcat(temp_buffer, requestURL, URL_ENCODING_OAUTH);

	strcat(temp_buffer, "&");

	// Copy alphabetized parameters
	for(i = 0; i < ord_req->numParams; i++)
	{
		// AND sign. Only add if not the first value
		if(i != 0)
			Url_strcat(temp_buffer, "&", URL_ENCODING_OAUTH);

		// Copy name
		Url_strcat(temp_buffer, ord_req->paramNames[i], URL_ENCODING_OAUTH);

		// = sign
		Url_strcat(temp_buffer, "=", URL_ENCODING_OAUTH);

		// Value
		Url_strcat(temp_buffer, ord_req->paramValues[i], URL_ENCODING_OAUTH);
	}


	// Release ord_req
	gsn_free(ord_req);

	// 'temp_buffer' should now hold the value of the signature base string.

	// Generate HMAC
	signature_dest = OAuth_HMAC_SHA1(temp_buffer, oauth_ctx);

	// Release buffer
	gsn_free(temp_buffer);

	// Add parameters to request structure
	OAuth_AddParam("oauth_signature", signature_dest, oauth_req);

	return signature_dest;
}

// Returns a time-stamp string in heap. Must be de-allocated by the user.
PRIVATE INT8*
OAuth_GetTimestamp()
{
	GSN_SYSTEM_TIME_T timestamp;
	UINT32 time;
	INT8* time_str;

	// Allocate memory
	time_str = gsn_malloc(21);

	// Populate timestamp
	timestamp = GsnTod_Get();

	// Convert to seconds
	time = (UINT32)SYSTIME_TO_SEC(timestamp);

	// Convert to ASCII, allocate new string size
	snprintf(time_str, 21, "%u", time);
	time_str[20] = 0;

	return time_str;
}

// Generates a nonce. Returns a heap-allocated string which must be de-allocated by the caller.
PRIVATE INT8*
OAuth_GetNonce()
{
	INT8* nonce;
	INT8* temp_nonce;
	size_t size;

	nonce = gsn_malloc(49);	// Random number is 12 characters long. worst-case scenario after url-encoding a base64 conversion is: 48 chars + null

	// Generate a random number
	os_get_random((UINT8*)nonce, 12);

	// Convert to base-64 to make it more readable
	temp_nonce = (INT8*)base64_encode((unsigned char*)nonce, 12, &size);

	// remove 0x0A at end of base64 string
	temp_nonce[--size] = 0x00;

	// Url-encode the base-64 encoded string
	nonce[0] = 0x00;
	Url_strcat(nonce, temp_nonce, URL_ENCODING_ALL);

	// Release temporary nonce
	gsn_free(temp_nonce);

	// Remove all percent characters. reuse temp_nonce as a pointer to the % characters location.
	while((temp_nonce = strchr(nonce, '%')) > 0)	// while a percent character is found
	{
		memmove(temp_nonce, &temp_nonce[1], strlen(&temp_nonce[1]) + 1); // shift string over the percent character. add one for null term. zerocopy.
	}

	// return the nonce
	return nonce;
}

/**
 * Generates a header from a request structure.
 */
PRIVATE VOID
OAuth_GenerateHeaderStr(INT8* header, OAUTH_REQ_T* oauth_req)
{
	UINT8 i;

	strcpy(header, "OAuth ");

	// For each item in the oauth request
	for(i = 0; TRUE; i++)
	{
		strcat(header, oauth_req->paramNames[i]); // Add name
		strcat(header, "=\"");
		strcat(header, oauth_req->paramValues[i]);
		strcat(header, "\"");

		// if not at at the end, add a comma
		if(i < oauth_req->numParams - 1)
			strcat(header, ",");
		else
			return;
	}
}

/**
 * Sorts the parameters for a request in alphabetical order
 */
PRIVATE VOID
OAuth_SortReq(OAUTH_REQ_T* output, const OAUTH_REQ_T* input)
{
	UINT8 i1;
	UINT8 i2;
	UINT8 val;

	// First, we memset over the output to clear it.
	memset(output, 0x00, sizeof(OAUTH_REQ_T));

	// First, we sort the current request in alphabetical order and store it in OAUTH_REQ_T
	for(i1 = 0; i1 < input->numParams; i1++)
	{
		// Find the first empty point in the second array
		for(val = 0; output->paramNames[val] != 0 && val < input->numParams; val++);

		for(i2 = i1 + 1; i2 < input->numParams; i2++)
		{
			// If the next request value comes alphabetically before the current value
			if(!isBefore(input->paramNames[i1], input->paramNames[i2]))
				for(val++; output->paramNames[val] != 0 && val < input->numParams; val++);	// increment val to the next empty spot in the output array.
		}

		// Copy new values in their alphabetically ordered spot.
		output->paramNames[val] = input->paramNames[i1];
		output->paramValues[val] = input->paramValues[i1];
	}

	output->numParams = input->numParams;
}

/** Generates an HMAC-SHA1 signature
 * Returns an allocated string which much be deallocated by the end user.
 * */
PRIVATE INT8*
OAuth_HMAC_SHA1(const INT8* source, const OAUTH_CTX_T* oauth_ctx)
{
	// Builds OAUTH HMAC key
	INT8* key;
	INT8* sha1_b64;
	UINT8* sha1_raw;
	size_t len = 0;

	// Allocate memory
	// 514 is worst case scenario for concatenation of consumerSecret and tokenSecret
	//   and '&' and null terminator.
	// consumerSecret and tokenSecret are both hard-coded to max size of 256.
	key = gsn_malloc(514);
	sha1_raw = gsn_malloc(20);

	key[0] = 0x00;

	Url_strcat(key, oauth_ctx->consumerSecret, URL_ENCODING_OAUTH);
	strcat(key, "&");

	// If token secret exists, copy it.
	if(oauth_ctx->tokenSecret)
		Url_strcat(key, oauth_ctx->tokenSecret, URL_ENCODING_OAUTH);

	hmac_sha1((UINT8*)key, strlen(key), (UINT8*)source, strlen(source), sha1_raw);

	// encode destination into base64
	sha1_b64 = (INT8*)base64_encode(sha1_raw, 20, &len);

	// Deallocate raw sha1, since we now have it in base64 form
	gsn_free(sha1_raw);

	// For some reason the last character is always 0x0A after base64 encoding. We remove it here. (bug in gsn functions?)
	sha1_b64[len - 1] = 0x00;

	// reuse 'key' variable to store url-encoded base-64 hmac. this is getting complicated.
	key[0] = 0x00;
	Url_strcat(key, sha1_b64, URL_ENCODING_OAUTH);

	// free original base64 string
	gsn_free(sha1_b64);


	return key;
}

/**
 *  Processes two name and value pairs and assigns their value within a context structure.
 *  @note name and valid can be de-allocated after passing to this function. This function will allocate new strings for use in oauth_ctx
 *  @param[in] name Name of auth header
 *  @param[in] value Value of auth header
 *  @param oauth_ctx context structure to be modified by the values
 */
PRIVATE GSN_STATUS
OAuth_ProcessResponse(INT8* name, INT8* value, OAUTH_CTX_T* oauth_ctx)
{
	INT8* tempToken;

	if(!strcasecmp(name, "oauth_token"))
	{
		// Decode in case of url-encoding
		tempToken = Url_decode(value, strlen(value));

		if(tempToken == 0x00) // if Url-decoding fails
		{
			return GSN_INVALID_PARAM;
		}
		else	// Url-decoding success.
		{
			// Copy to permanent token key location.
			GSN_STATUS retval;

			// If we have enough room
			if(strlen(tempToken) < oauth_ctx->tokenKeyMaxLen)
			{
				// Copy into permanent location
				strcpy(oauth_ctx->tokenKey, tempToken);
				retval = GSN_SUCCESS;
			}
			else // Buffer overflow
			{
				retval = GSN_BUFFER_OVERFLOW;
			}

			// Free temporary token
			gsn_free(tempToken);

			// Exit
			return retval;
		}
	}
	else if(!strcasecmp(name, "oauth_token_secret"))
	{
		// Decode in case of url-encoding
		tempToken = Url_decode(value, strlen(value));

		if(tempToken == 0x00)
		{
			return GSN_INVALID_PARAM;
		}
		else // URL-Decoding success
		{
			// Copy to permanent key location
			GSN_STATUS retval;

			// If we have enough room
			if(strlen(tempToken) < oauth_ctx->tokenSecretMaxLen)
			{
				// Copy to permanent location
				strcpy(oauth_ctx->tokenSecret, tempToken);
				retval = GSN_SUCCESS;
			}
			else // buffer overflow
			{
				retval = GSN_BUFFER_OVERFLOW;
			}

			// free temporary buffer
			gsn_free(tempToken);

			// exit
			return retval;
		}
	}

	return GSN_INVALID_PARAM;
}

/**
 * If first string comes before second string when sorting in alphabetical order
 */
PRIVATE INLINE UINT8
isBefore(const INT8* str1, const INT8* str2)
{
	UINT32 i;

	for(i = 0; 1; i++)
	{
		// if the first character is not equal to the second character
		if(str1[i] != str2[i])
		{
			// compare the characters to see who won
			if(str2[i] > str1[i])
				return 0x01;
			else
				return 0x00;
		}
		else // str1[i] == str2[i]
		{
			if(str1[i] == 0x00) // if we've reached a null character
				return 0x00;	// Just return zero.
		}
	}
}
