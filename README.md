# README #

### What is this repository for? ###

* This repository contains code samples in various languages to demonstrate to potential employers.
* All code samples in this directory were written, from scratch, by Andrew Klitzke
* These are real, unmodified code samples, many of which are deployed in actual products somewhere.
* This repository contains no intellectual property and all potentially identifying information has been removed. 

### Can I run any of these? ###

* Unfortunately, all of these files are a part of a larger solution and cannot be run by themselves.

### Do you have any more? Any in language xyz? ###

* Sure! Email me and I can let you know!